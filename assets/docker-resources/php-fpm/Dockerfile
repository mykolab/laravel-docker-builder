ARG PHP_VERSION

FROM php:${PHP_VERSION}-fpm-alpine3.14

LABEL maintainer="Tallium <dev@tallium.com>"

# Install software
RUN set -eux \
&& apk --update --no-cache add wget \
  bash \
  curl \
  git \
  build-base \
  libmcrypt-dev \
  libxml2-dev \
  pcre-dev \
  zlib-dev \
  autoconf \
  cyrus-sasl-dev \
  libgsasl-dev \
  oniguruma-dev \
  libressl \
  libressl-dev \
  libpng-dev \
  procps

RUN set -eux; \
    docker-php-ext-install bcmath; \
    docker-php-ext-install soap; \
    docker-php-ext-install pcntl; \
    docker-php-ext-install mbstring

RUN pecl install -o -f redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable redis

RUN set -eux \
    && apk --update --no-cache add libzip-dev zip unzip; \
    docker-php-ext-install zip

ARG INSTALL_INTL=false
RUN if [ ${INSTALL_INTL} = true ]; then \
    apk --update --no-cache add icu-dev; \
    docker-php-ext-install intl \
;fi

ARG INSTALL_PGSQL=false
RUN if [ ${INSTALL_PGSQL} = true ]; then \
    set -ex; \
    apk --no-cache add postgresql-dev; \
    docker-php-ext-install pdo_pgsql \
;fi

ARG INSTALL_MYSQL=false
RUN if [ ${INSTALL_MYSQL} = true ]; then \
    # Install the mysql extension
    docker-php-ext-install pdo_mysql \
;fi

ARG INSTALL_MONGO=false
RUN if [ ${INSTALL_MONGO} = true ]; then \
    # Install the mongodb extension \
    pecl install mongodb; \
    docker-php-ext-enable mongodb \
    php -m | grep -oiE '^mongodb$' \
;fi

ARG INSTALL_WKHTMLTOPDF=false
RUN if [ ${INSTALL_WKHTMLTOPDF} = true ]; then \
    apk add --update --no-cache \
      libgcc \
      libstdc++ \
      libx11 \
      glib \
      libxrender \
      libxext \
      libintl \
      ttf-dejavu \
      ttf-droid \
      ttf-freefont \
      ttf-liberation \
      wkhtmltopdf \
;fi

ARG INSTALL_IMAGEMAGICK=false
RUN if [ ${INSTALL_IMAGEMAGICK} = true ]; then \
    apk add --update --no-cache imagemagick-dev imagemagick \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
;fi

ARG INSTALL_OPCACHE=false
RUN if [ ${INSTALL_OPCACHE} = true ]; then \
    docker-php-ext-install opcache \
;fi
COPY ./opcache.ini /usr/local/etc/php/conf.d/opcache.ini

# Install and setup xdebug
ARG INSTALL_XDEBUG=false
RUN if [ ${INSTALL_XDEBUG} = true ]; then \
  pecl install xdebug-3.1.1; \
  docker-php-ext-enable xdebug \
;fi

COPY ./xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
COPY ./laravel.ini /usr/local/etc/php/conf.d
COPY ./xlaravel.pool.conf /usr/local/etc/php-fpm.d/

# Configure locale.
ARG LOCALE=POSIX
ENV LC_ALL ${LOCALE}

WORKDIR /var/www

CMD ["php-fpm"]

EXPOSE 9000
