#!/bin/bash

if [[ $POSTGRES_USER == 'test_user' ]]; then
    echo "Test DB already exists. Ignored."
    exit 0
fi

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER test_user WITH PASSWORD 'password';
    CREATE DATABASE test_db;
    GRANT ALL PRIVILEGES ON DATABASE test_db TO test_user;
EOSQL
