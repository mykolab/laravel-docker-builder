#!/bin/bash

set -e

if ! [ -x "$(command -v docker-compose)" ]; then
    error 'Docker-compose is not installed.' >&2
    exit 1
fi

if ! [ -f "./.env" ]; then
    error '.env file is not found.' >&2
    exit 1
fi

source ./.env

# All available application domains. Basically means how many nginx config files we use.
applications_domains=("${BACKEND_APP_DOMAINS}" "${FRONTEND_APP_DOMAINS}")
email="${CERTBOT_EMAIL}" # Adding a valid address is strongly recommended
data_path="./data/certbot"
rsa_key_size=4096
staging=0 # Set to 1 if you're testing your setup to avoid hitting request limits

if [ -d "$data_path" ]; then
    read -p "Existing data found for ssl certificates. Continue and replace existing certificate? (y/N) " decision
    if [ "$decision" != "Y" ] && [ "$decision" != "y" ]; then
        exit
    fi
fi

func_inc=0
info() {
  func_inc=$((func_inc+1));
  local color='\033[0;32m'
  local nc='\033[0m';
  echo -e "\n${color}$func_inc. $1${nc}";
}

error() {
  local color='\033[0;31m'
  local nc='\033[0m';
  echo -e "\n${color}Error: $1${nc}";
}

function install_dummy_certificates() {
    local domains_str="$1"
    IFS=' ' read -r -a domains <<< "$domains_str"
    local main_domain="${domains[0]}"

    info "Creating dummy certificate for $domains_str ..."
    path="/etc/letsencrypt/live/$main_domain"
    mkdir -p "$data_path/conf/live/$main_domain"
    docker-compose run --rm --entrypoint "\
        openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
            -keyout '$path/privkey.pem' \
            -out '$path/fullchain.pem' \
            -subj '/CN=localhost'" certbot
}

function delete_dummy_certificates() {
    local domains_str="$1"
    IFS=' ' read -r -a domains <<< "$domains_str"
    local main_domain="${domains[0]}"

    info "Deleting dummy certificate for $domains_str ..."
    docker-compose run --rm --entrypoint "\
        rm -Rf /etc/letsencrypt/live/$main_domain && \
        rm -Rf /etc/letsencrypt/archive/$main_domain && \
        rm -Rf /etc/letsencrypt/renewal/$main_domain.conf" certbot
}

function request_letsencrypt_certificate() {
    local domains_str="$1"
    IFS=' ' read -r -a domains <<< "$domains_str"
    local main_domain="${domains[0]}"

    info "Requesting Let's Encrypt certificate for $domains_str ..."
    #Join $domains to -d args
    local domain_args=""
    for domain in "${domains[@]}"; do
        domain_args="$domain_args -d $domain"
    done

    # Select appropriate email arg
    case "$email" in
        "") email_arg="--register-unsafely-without-email" ;;
        *) email_arg="--email $email" ;;
    esac

    # Enable staging mode if needed
    if [ $staging != "0" ]; then staging_arg="--staging"; fi

    docker-compose run --rm --entrypoint "\
      certbot certonly --webroot -w /var/www/certbot \
        $staging_arg \
        $email_arg \
        $domain_args \
        --rsa-key-size $rsa_key_size \
        --agree-tos \
        --force-renewal" certbot
}

if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
    info "Downloading recommended TLS parameters ..."
    mkdir -p "$data_path/conf"
    curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "$data_path/conf/options-ssl-nginx.conf"
    curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "$data_path/conf/ssl-dhparams.pem"
fi

for application_domains in "${applications_domains[@]}"; do
    if [ ! -z "$application_domains" ]; then
        install_dummy_certificates "${application_domains}"
    fi
done

info "Starting nginx ..."
docker-compose up --force-recreate -d nginx

for application_domains in "${applications_domains[@]}"; do
    if [ ! -z "$application_domains" ]; then
        delete_dummy_certificates "${application_domains}"
    fi
done

for application_domains in "${applications_domains[@]}"; do
    if [ ! -z "$application_domains" ]; then
        request_letsencrypt_certificate "${application_domains}"
    fi
done

info "Reloading nginx ..."
docker-compose exec nginx nginx -s reload
