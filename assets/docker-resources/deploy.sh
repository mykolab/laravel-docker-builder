#!/bin/bash

set -e

if ! [ -x "$(command -v docker-compose)" ]; then
    echo 'Docker-compose is not installed.' >&2
    exit 1
fi

if ! [ -f "./.env" ]; then
    echo '.env file is not found.' >&2
    exit 1
fi

env_name="$1"
certbot_data_path="./data/certbot"

func_inc=0
info() {
  func_inc=$((func_inc+1));
  local color='\033[0;32m'
  local nc='\033[0m';
  echo -e "\n${color}$func_inc. $1${nc}";
}

error() {
  local color='\033[0;31m'
  local nc='\033[0m';
  echo -e "\n${color}Error: $1${nc}";
}

if [ -z "$env_name" ]; then
    error 'No environment name was not passed as argument.' >&2
    exit 1
fi

if [ ! -d "$certbot_data_path" ]; then
    info "Installing ssl certificates"
    sh ./init-letsencrypt.sh
else
    info "Skip installing ssl certificates"
fi

if ! [ -f "./.env.${env_name}" ]; then
    error ".env.${env_name} file is not found." >&2
    exit 1
fi

info "Copying .env.${env_name} to .env"
cp .env.${env_name} .env

info "Run docker containers"
docker-compose up -d

info "Setup application"
docker-compose exec -T -u tallium workspace bash -ci "set -ex
    composer install
    php artisan install
    npm install
"

info "Done"
