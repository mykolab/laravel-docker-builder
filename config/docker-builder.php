<?php

use Mykolab\LaravelDockerBuilder\DockerServices\CertbotDockerService;
use Mykolab\LaravelDockerBuilder\DockerServices\PhpWorkerDockerService;
use Mykolab\LaravelDockerBuilder\DockerServices\NginxDockerService;
use Mykolab\LaravelDockerBuilder\DockerServices\PhpFpmDockerService;
use Mykolab\LaravelDockerBuilder\DockerServices\PostgresDockerService;
use Mykolab\LaravelDockerBuilder\DockerServices\RedisDockerService;
use Mykolab\LaravelDockerBuilder\DockerServices\WorkspaceDockerService;
use Mykolab\LaravelDockerBuilder\EnvironmentConfigurations\CommonEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\EnvironmentConfigurations\PhpDefaultEnvironmentConfiguration;

return [
    'docker_destination_folder_name' => 'docker',
    'docker_compose_version' => 3.9,
    'available_php_versions' => [
        '8.1',
    ],
    'node_version' => 16,
    'postgres_version' => 14.1,
    'redis_version' => 6.2,
    'nginx_version' => 1.21,
    'certbot_email' => 'dev@tallium.com',
    'site_templates' => [
        'backend' => 'backend.conf.template',
        'frontend' => 'frontend.conf.template',
    ],
    'environment_configurations' => [
        'default' => [
            CommonEnvironmentConfiguration::class,
            PhpDefaultEnvironmentConfiguration::class,
        ],
    ],
    'docker_services' => [
        'available' => [
            WorkspaceDockerService::class,
            NginxDockerService::class,
            PhpFpmDockerService::class,
            PhpWorkerDockerService::class,
            PostgresDockerService::class,
            RedisDockerService::class,
            CertbotDockerService::class,
        ],
        'default' => [
            WorkspaceDockerService::class,
            NginxDockerService::class,
            PhpFpmDockerService::class,
            PhpWorkerDockerService::class,
            PostgresDockerService::class,
            RedisDockerService::class,
        ],
    ],
];
