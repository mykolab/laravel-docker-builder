<?php

namespace Mykolab\LaravelDockerBuilder\StubReplacers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasVolume;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;

class DockerFileStubReplacer
{
    public function replace(string $stubContent, DockerServiceCollection $dockerServices): string
    {
        $dockerComposeServicesStubs = $dockerServices
            ->map(
                fn(DockerService $dockerService) => File::get(
                    DockerAssets::getDockerComposeYmlStubsPath($dockerService->getName())
                )
            );

        $volumesCollection = $dockerServices
            ->whereHasVolume()
            ->map(function (DockerService|HasVolume $dockerService) {
                $volume = Str::of($dockerService->getName())->prepend("  ")->append(":\n");
                foreach ($dockerService->getVolumeOptions() as $name => $value) {
                    $volume .= "    $name: $value\n";
                }

                return $volume;
            });

        $volumes = '';
        if ($volumesCollection->isNotEmpty()) {
            $volumes = "\nvolumes:\n".$volumesCollection->implode("\n");
        }

        $services = "services:\n".$dockerComposeServicesStubs->implode("\n");

        return (string) Str::of($stubContent)
            ->replace('{{volumes}}', $volumes)
            ->replace('{{services}}', $services);
    }
}
