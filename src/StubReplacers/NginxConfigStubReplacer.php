<?php

namespace Mykolab\LaravelDockerBuilder\StubReplacers;

use Illuminate\Support\Str;
use Illuminate\Support\Stringable;

class NginxConfigStubReplacer
{
    public function replace(string $stubContent, string $envName, array $domains): string
    {
        $domainsStr = implode(' ', $domains);

        $sslDomain = $domains[0];
        $sslRedirects = $this->getHttpsRedirects($domains);

        $nginxConfig = Str::of($stubContent)->replace(
            [
                '{{domains}}',
                '{{ssl_domain}}',
                '{{https_redirects}}',
            ],
            [
                $domainsStr,
                $sslDomain,
                $sslRedirects,
            ],
        );

        return $this->replaceEnvironmentPatterns($envName, $nginxConfig);
    }

    /**
     * Disable ssl configuration and redirection for local nginx config files.
     * Cleanup configuration comments for not local environments.
     *
     * @param string $envName
     * @param Stringable $nginxConfig
     * @return Stringable
     */
    private function replaceEnvironmentPatterns(string $envName, Stringable $nginxConfig): Stringable
    {
        if ($envName === 'local') {
            return $nginxConfig
                ->replaceMatches('/(\s+#start_release_configuration)(.+?)(#end_release_configuration)/s', '') // remove release config
                ->replaceMatches('/(\s{4}|)#start_local_configuration|(\s{4}|)#end_local_configuration\n/', ''); // remove local config comments
        }

        return $nginxConfig
            ->replaceMatches('/(\s{4}|)#start_release_configuration|(\s{4}|)#end_release_configuration\n/', '') // remove local config
            ->replaceMatches('/(\s+#start_local_configuration)(.+?)(#end_local_configuration)/s', ''); // remove release config comments
    }

    private function getHttpsRedirects(array $domains): string
    {
        $sslRedirectTemplate = <<<END
            if (\$host = %s) {
                return 301 https://\$host\$request_uri;
            }
        END;

        return collect($domains)
            ->map(
                fn(string $domain) => sprintf($sslRedirectTemplate, $domain)
            )
            ->implode("\n");
    }
}
