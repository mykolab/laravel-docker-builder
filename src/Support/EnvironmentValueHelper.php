<?php

namespace Mykolab\LaravelDockerBuilder\Support;

use Closure;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Str;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;

class EnvironmentValueHelper
{
    public function __construct(
        private readonly ShellCommandsHelper $shellExecHelper
    ) {}

    public function getRegistryUrlFromGit(): ?string
    {
        $remoteOriginUrl = $this->shellExecHelper->gitRemoteOriginUrl();
        if (! $remoteOriginUrl) {
            return null;
        }

        return (string) Str::of($remoteOriginUrl)
            ->trim("\n")
            ->replace('gitlab.com:', 'gitlab.com/')
            ->match('/gitlab\.com.*/')
            ->remove('.git')
            ->prepend('registry.');
    }

    /**
     * @param int $length
     * @return Closure(): string
     */
    public function getPasswordGeneratorClosure(int $length = 24): Closure
    {
        return fn(string $envName): string => Str::random($length);
    }

    /**
     * @throws BindingResolutionException
     */
    public function shouldInstallPostgres(): bool
    {
        $dockerFactory = app()->make(DockerServiceFactory::class);

        return $dockerFactory->getInstalledDockerServices()->containsDockerService('postgres');
    }

    /**
     * @throws BindingResolutionException
     */
    public function shouldInstallMySql(): bool
    {
        $dockerFactory = app()->make(DockerServiceFactory::class);

        return $dockerFactory->getInstalledDockerServices()->containsDockerService('mysql');
    }
}
