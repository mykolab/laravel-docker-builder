<?php

namespace Mykolab\LaravelDockerBuilder\Support;

class DockerAssetsHelper
{
    public function getDockerDestinationPath(?string $relativePath = null): string
    {
        $relativePath = trim($relativePath, '/');

        return base_path(config('docker-builder.docker_destination_folder_name')).'/'.$relativePath;
    }

    public function getDockerResourcesPath(?string $relativePath = null): string
    {
        $relativePath = trim($relativePath, '/');

        return $this->getPackageBasePath('assets/docker-resources/'.$relativePath);
    }

    public function getDockerComposeYmlStubsPath(string $stubName): string
    {
        return $this->getPackageBasePath("assets/service-stubs/$stubName.yml.stub");
    }

    private function getPackageBasePath(?string $relativePath = null): string
    {
        $packageBasePath = __DIR__.'/../../';

        return $packageBasePath . trim($relativePath, '/');
    }
}
