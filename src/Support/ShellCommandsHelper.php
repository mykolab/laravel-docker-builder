<?php

namespace Mykolab\LaravelDockerBuilder\Support;

class ShellCommandsHelper
{
    public function gitRemoteOriginUrl(): ?string
    {
        return shell_exec(
            sprintf('cd %s && git remote get-url origin', base_path())
        );
    }
}
