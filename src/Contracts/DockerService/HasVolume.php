<?php

namespace Mykolab\LaravelDockerBuilder\Contracts\DockerService;

interface HasVolume
{
    public function getVolumeOptions(): array;
}
