<?php

namespace Mykolab\LaravelDockerBuilder\Contracts\DockerService;

use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;

interface HasEnvironmentConfiguration
{
    public function getEnvironmentConfiguration(): EnvironmentConfiguration;
}
