<?php

namespace Mykolab\LaravelDockerBuilder\Contracts\DockerService;

interface DockerService
{
    public function getName(): string;
}
