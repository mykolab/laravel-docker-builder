<?php

namespace Mykolab\LaravelDockerBuilder\Contracts\DockerService;

interface HasAfterDockerServiceInstalledAction
{
    public function afterDockerServiceInstalled(): void;
}
