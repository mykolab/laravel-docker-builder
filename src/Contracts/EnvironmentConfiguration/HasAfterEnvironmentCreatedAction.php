<?php

namespace Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration;

use Mykolab\LaravelDockerBuilder\DTO\Environment;

interface HasAfterEnvironmentCreatedAction
{
    public function afterEnvironmentCreated(Environment $environment): void;
}
