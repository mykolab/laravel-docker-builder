<?php

namespace Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration;

use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;

interface EnvironmentConfiguration
{
    /**
     * @return array|EnvVariableValueProvider[]
     */
    public function getValueProviders(): array;
}
