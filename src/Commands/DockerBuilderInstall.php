<?php

namespace Mykolab\LaravelDockerBuilder\Commands;

use Illuminate\Console\Command;
use Mykolab\LaravelDockerBuilder\AssetGenerators\CommonFilesGenerator;
use Mykolab\LaravelDockerBuilder\AssetGenerators\DockerFilesGenerator;
use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Exceptions\DockerBuilderException;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;

class DockerBuilderInstall extends Command
{
    public $signature = 'docker-builder:install';

    public $description = 'Build and install docker configuration';

    public function __construct(
        private readonly DockerServiceFactory $dockerServiceFactory,
        private readonly CommonFilesGenerator $commonFilesGenerator,
        private readonly DockerFilesGenerator $dockerFilesGenerator
    ) {
        parent::__construct();
    }

    public function handle(): int
    {
        $this->info('Starting docker files generation.');

        $dockerServices = $this->askDockerConfiguration();

        try {
            $this->dockerFilesGenerator->generate($dockerServices);
            $this->commonFilesGenerator->generate();
            $this->info('Docker files was successfully generated.');

            $confirmationQuestion = 'Would you like to create new environment?';
            if ($this->confirm($confirmationQuestion, true)) {
                $this->call('docker-builder:create-environment');
            }
        } catch (DockerBuilderException $e) {
            $this->error(
                sprintf('Docker files generation failed with error: %s', $e->getMessage())
            );

            return self::FAILURE;
        }

        return self::SUCCESS;
    }

    private function askDockerConfiguration(): DockerServiceCollection
    {
        $defaultDockerServices = $this->dockerServiceFactory->getDefaultDockerServices();
        $dockerServiceNames = $defaultDockerServices->map->getName();

        $confirmationQuestion = sprintf('Are you sure you want to install docker services with default configuration: <fg=yellow>%s</> ?', $dockerServiceNames->implode(', '));
        if (! $this->confirm($confirmationQuestion, true)) {
            return $this->chooseDockerServices();
        }

        return $defaultDockerServices;
    }

    private function chooseDockerServices(): DockerServiceCollection
    {
        $dockerServices = $this->dockerServiceFactory->getDockerServices();
        $dockerServiceNames = $dockerServices->map->getName()->toArray();

        $dockerServicesQuestion = 'Choose docker services:';
        $selectedDockerServiceNames = $this->choice(question: $dockerServicesQuestion, choices: $dockerServiceNames, multiple: true);

        $confirmationQuestion = sprintf('Are you sure you want to install such docker services: <fg=yellow>%s</> ?', implode(',', $selectedDockerServiceNames));
        if (! $this->confirm($confirmationQuestion)) {
            return $this->chooseDockerServices();
        }

        return $dockerServices->whereNameIn($selectedDockerServiceNames);
    }
}
