<?php

namespace Mykolab\LaravelDockerBuilder\Commands;

use Illuminate\Console\Command;
use Mykolab\LaravelDockerBuilder\AssetGenerators\EnvFileGenerator;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\EnvironmentActionDispatcher;
use Mykolab\LaravelDockerBuilder\EnvironmentService;

class DockerBuilderCreateEnvironment extends Command
{
    public $signature = 'docker-builder:create-environment';

    protected $description = 'Create new environment. Generate environment files.';

    public function __construct(
        private readonly EnvironmentService $environmentService,
        private readonly EnvironmentActionDispatcher $environmentActionDispatcher,
        private readonly EnvFileGenerator $environmentFileGenerator
    )
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $envNameQuestion = 'Enter environment name (it should be sluggable word, ex.: <fg=yellow>local</>, <fg=yellow>staging</>, <fg=yellow>production</>)';
        $envName = $this->ask($envNameQuestion);
        if ($this->environmentService->isEnvironmentExists($envName)) {
            $this->error("Cannot create '$envName' environment. It is already exists.");

            return self::FAILURE;
        }

        $this->info("Fill variables for <fg=yellow>$envName</> environment:");

        return $this->fillEnvVariables($envName);
    }

    private function fillEnvVariables(string $envName): bool
    {
        $envVariablesConfigurator = $this->environmentService->newEnvironmentConfigurator($envName);

        while ($envVariableValueProvider = $envVariablesConfigurator->nextInputableVariableValueProvider()) {
            $envVariableValue = $this->askForVariableValue($envName, $envVariableValueProvider);
            // TODO: Implement validation. Do we need it?
            $envVariablesConfigurator->addEnvVariable($envVariableValueProvider, $envVariableValue);
        }

        $environment = $envVariablesConfigurator->getEnvironment();

        $this->info('Generating environment files...');
        if (! $this->environmentFileGenerator->generate($environment)) {
            $this->error('Cannot generate env file.');

            return self::FAILURE;
        }

        $this->environmentActionDispatcher->dispatchAfterEnvironmentCreatedAction($environment);

        $this->info(
            sprintf('Environment <fg=yellow>%s</> was successfully created.', $envName)
        );

        return self::SUCCESS;
    }

    private function askForVariableValue(string $envName, EnvVariableValueProvider $envVariableValueProvider): mixed
    {
        $question = sprintf(
            '%s [<fg=yellow>%s</>]',
            $envVariableValueProvider->getDescription(),
            $envVariableValueProvider->getName()
        );

        if ($envVariableValueProvider->hasChoices()) {
            return $this->choice($question, $envVariableValueProvider->getChoices());
        } else {
            return $this->ask($question, $envVariableValueProvider->resolveDefaultValue($envName));
        }
    }
}
