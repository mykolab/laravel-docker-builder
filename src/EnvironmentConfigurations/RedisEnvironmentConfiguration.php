<?php

namespace Mykolab\LaravelDockerBuilder\EnvironmentConfigurations;

use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\Support\EnvironmentValueHelper;

class RedisEnvironmentConfiguration implements EnvironmentConfiguration
{
    public function __construct(
        private readonly EnvironmentValueHelper $environmentValueHelper
    ) {}

    public function getValueProviders(): array
    {
        return [
            EnvVariableValueProvider::make('REDIS_VERSION')->defaultValue(config('docker-builder.redis_version')),
            EnvVariableValueProvider::make('REDIS_PASSWORD')->requireInput()->differentForAllEnvironments()->defaultValue($this->environmentValueHelper->getPasswordGeneratorClosure()),
            EnvVariableValueProvider::make('REDIS_PORT')->defaultValue('127.0.0.1:6379')->emptyLine(),
        ];
    }
}
