<?php

namespace Mykolab\LaravelDockerBuilder\EnvironmentConfigurations;

use Mykolab\LaravelDockerBuilder\AssetGenerators\NginxConfigGenerator;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\HasAfterEnvironmentCreatedAction;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariable;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\Support\EnvironmentValueHelper;

class PostgresEnvironmentConfiguration implements EnvironmentConfiguration
{
    public function __construct(
        private readonly EnvironmentValueHelper $environmentValueHelper
    ) {}

    public function getValueProviders(): array
    {
        return [
            EnvVariableValueProvider::make('POSTGRES_VERSION')->defaultValue(config('docker-builder.postgres_version')),
            EnvVariableValueProvider::make('POSTGRES_DB')->requireInput()->defaultValue('default')->differentForAllEnvironments(),
            EnvVariableValueProvider::make('POSTGRES_USER')->requireInput()->defaultValue('default')->differentForAllEnvironments(),
            EnvVariableValueProvider::make('POSTGRES_PASSWORD')->requireInput()->defaultValue($this->environmentValueHelper->getPasswordGeneratorClosure())->differentForAllEnvironments(),
            EnvVariableValueProvider::make('POSTGRES_PORT')->defaultValue('127.0.0.1:5432'),
            EnvVariableValueProvider::make('POSTGRES_ENTRYPOINT_INITDB')->defaultValue('./postgres/docker-entrypoint-initdb.d')->emptyLine(),
        ];
    }
}
