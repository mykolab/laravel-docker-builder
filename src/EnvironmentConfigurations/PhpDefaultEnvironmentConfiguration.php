<?php

namespace Mykolab\LaravelDockerBuilder\EnvironmentConfigurations;

use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\Support\EnvironmentValueHelper;

class PhpDefaultEnvironmentConfiguration implements EnvironmentConfiguration
{
    public function __construct(
        private readonly EnvironmentValueHelper $environmentValueHelper
    ) {}

    public function getValueProviders(): array
    {
        $phpVersions = config('docker-builder.available_php_versions', []);

        return [
            EnvVariableValueProvider::make('PHP_VERSION')
                ->requireInput()
                ->choices($phpVersions)
                ->note(__('docker-builder::env_configuration.variables.note.PHP_VERSION', ['versions' => implode(', ', $phpVersions)])),
            EnvVariableValueProvider::make('INSTALL_PGSQL')->defaultValue($this->environmentValueHelper->shouldInstallPostgres()),
            EnvVariableValueProvider::make('INSTALL_MYSQL')->defaultValue($this->environmentValueHelper->shouldInstallMySql()),
            EnvVariableValueProvider::make('INSTALL_MONGO')->defaultValue(false),
            EnvVariableValueProvider::make('INSTALL_INTL')->defaultValue(false),
            EnvVariableValueProvider::make('INSTALL_WKHTMLTOPDF')->defaultValue(false),
            EnvVariableValueProvider::make('INSTALL_IMAGEMAGICK')->defaultValue(false),
            EnvVariableValueProvider::make('INSTALL_OPCACHE')->defaultValue(false)->emptyLine(),
        ];
    }
}
