<?php

namespace Mykolab\LaravelDockerBuilder\EnvironmentConfigurations;

use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;

class PhpFpmEnvironmentConfiguration implements EnvironmentConfiguration
{
    public function getValueProviders(): array
    {
        return [
            EnvVariableValueProvider::make('PHP_FPM_INSTALL_XDEBUG')->defaultValue(false),
            EnvVariableValueProvider::make('PHP_FPM_XDEBUG_PORT')->defaultValue('127.0.0.1:9003')->emptyLine(),
        ];
    }
}
