<?php

namespace Mykolab\LaravelDockerBuilder\EnvironmentConfigurations;

use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;

class WorkspaceEnvironmentConfiguration implements EnvironmentConfiguration
{
    public function getValueProviders(): array
    {
        return [
            EnvVariableValueProvider::make('WORKSPACE_SSH_PORT')->defaultValue('127.0.0.1:2222'),
            EnvVariableValueProvider::make('WORKSPACE_NODE_VERSION')->defaultValue(config('docker-builder.node_version')),
        ];
    }
}
