<?php

namespace Mykolab\LaravelDockerBuilder\EnvironmentConfigurations;

use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;

class CertbotEnvironmentConfiguration implements EnvironmentConfiguration
{
    public function getValueProviders(): array
    {
        return [
            EnvVariableValueProvider::make('CERTBOT_EMAIL')->requireInput()->defaultValue(config('docker-builder.certbot_email')),
        ];
    }
}
