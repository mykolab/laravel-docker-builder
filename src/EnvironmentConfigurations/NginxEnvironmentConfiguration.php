<?php

namespace Mykolab\LaravelDockerBuilder\EnvironmentConfigurations;

use Mykolab\LaravelDockerBuilder\AssetGenerators\NginxConfigGenerator;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\HasAfterEnvironmentCreatedAction;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariable;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;

class NginxEnvironmentConfiguration implements EnvironmentConfiguration, HasAfterEnvironmentCreatedAction
{
    public function __construct(
        private readonly NginxConfigGenerator $nginxConfigGenerator
    ) {}

    public function getValueProviders(): array
    {
        return [
            EnvVariableValueProvider::make('NGINX_VERSION')->defaultValue(config('docker-builder.nginx_version')),
            EnvVariableValueProvider::make('NGINX_HOST_LOG_PATH')->defaultValue('./logs/nginx/'),
            EnvVariableValueProvider::make('NGINX_SITE_TEMPLATES_PATH')->defaultValue('./nginx/site-templates/${ENV_NAME}')->emptyLine(),
            EnvVariableValueProvider::make('CERTBOT_WORKDIR_HOST_PATH')->defaultValue('./data/certbot/www'),
            EnvVariableValueProvider::make('CERTBOT_LETS_ENCRYPT_HOST_PATH')->defaultValue('./data/certbot/conf'),
            EnvVariableValueProvider::make('CERTBOT_HOST_LOG_PATH')->defaultValue('./logs/letsencrypt')->emptyLine(),
        ];
    }

    public function afterEnvironmentCreated(Environment $environment): void
    {
        $backendAppDomains = $environment->envVariables
            ->firstWhere('name', 'BACKEND_APP_DOMAINS')
            ?->value;

        if ($backendAppDomains) {
            $this->nginxConfigGenerator->createNginxConfigFromStub(
                $environment->name,
                explode(' ', $backendAppDomains),
                config('docker-builder.site_templates.backend')
            );
        }

        $frontEndAppDomains = $environment->envVariables
            ->firstWhere('name', 'FRONTEND_APP_DOMAINS')
            ?->value;

        if ($frontEndAppDomains) {
            $this->nginxConfigGenerator->createNginxConfigFromStub(
                $environment->name,
                explode(' ', $frontEndAppDomains),
                config('docker-builder.site_templates.frontend')
            );
        }
    }
}
