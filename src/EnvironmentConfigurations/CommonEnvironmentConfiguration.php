<?php

namespace Mykolab\LaravelDockerBuilder\EnvironmentConfigurations;

use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\Support\EnvironmentValueHelper;

class CommonEnvironmentConfiguration implements EnvironmentConfiguration
{
    public function __construct(
        private readonly EnvironmentValueHelper $environmentValueHelper
    ) {}

    public function getValueProviders(): array
    {
        return [
            EnvVariableValueProvider::make('COMPOSE_PROJECT_NAME')->requireInput(),
            EnvVariableValueProvider::make('IMAGE_REGISTRY')->requireInput()->defaultValue($this->environmentValueHelper->getRegistryUrlFromGit()),
            EnvVariableValueProvider::make('IMAGE_TAG')->defaultValue('${ENV_NAME}'),
            EnvVariableValueProvider::make('APP_CODE_PATH_HOST')->defaultValue('../'),
            EnvVariableValueProvider::make('APP_CODE_PATH_CONTAINER')->defaultValue('/var/www'),
            EnvVariableValueProvider::make('APP_CODE_CONTAINER_FLAG')->defaultValue(':cached'),
            EnvVariableValueProvider::make('DATA_PATH_HOST')->defaultValue('~/.tallium-laravel-docker/data/${COMPOSE_PROJECT_NAME}'),
            EnvVariableValueProvider::make('PHP_IDE_CONFIG')->defaultValue('serverName=tallium-laravel-docker')->emptyLine(),

            EnvVariableValueProvider::make('BACKEND_APP_DOMAINS')->requireInput()->differentForAllEnvironments(),
            EnvVariableValueProvider::make('FRONTEND_APP_DOMAINS')->requireInput()->differentForAllEnvironments(),
        ];
    }
}
