<?php

namespace Mykolab\LaravelDockerBuilder;

use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\Factories\EnvironmentFactory;

class EnvironmentService
{
    public function __construct(
        private readonly EnvironmentFactory $environmentFactory
    ) {}

    public function isEnvironmentExists(mixed $envName): bool
    {
        return $this->environmentFactory->getInstalledEnvironments()
            ->where('name', $envName)
            ->isNotEmpty();
    }

    public function newEnvironmentConfigurator(string $envName): EnvironmentConfigurator
    {
        return new EnvironmentConfigurator(
            $envName,
            $this->environmentFactory->getInstalledEnvironments(),
            $this->environmentFactory->getInstalledEnvVariableValueProviders()
        );
    }
}
