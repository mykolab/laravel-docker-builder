<?php

namespace Mykolab\LaravelDockerBuilder;

use Illuminate\Foundation\Application;
use Mykolab\LaravelDockerBuilder\Commands\DockerBuilderCreateEnvironment;
use Mykolab\LaravelDockerBuilder\Commands\DockerBuilderInstall;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;
use Mykolab\LaravelDockerBuilder\Factories\EnvironmentFactory;
use Mykolab\LaravelDockerBuilder\Support\DockerAssetsHelper;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class LaravelDockerBuilderServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('laravel-docker-builder')
            ->hasConfigFile()
            ->hasTranslations()
            ->hasCommands(DockerBuilderInstall::class, DockerBuilderCreateEnvironment::class);
    }

    public function packageRegistered()
    {
        $this->app->singleton(DockerServiceFactory::class);
        $this->app->singleton(EnvironmentFactory::class, function (Application $app) {
            $defaultEnvConfigurations = array_map(
                fn(string $envConfigurationClass) => $app->make($envConfigurationClass),
                config('docker-builder.environment_configurations.default', [])
            );

            return new EnvironmentFactory(
                $app->make(DockerServiceFactory::class),
                $app->make(DotEnvParser::class),
                $defaultEnvConfigurations
            );
        });

        $this->app->when(DockerServiceFactory::class)
            ->needs(DockerService::class)
            ->give(
                config('docker-builder.docker_services.available')
            );

        $this->app->singleton('docker-assets-helper', DockerAssetsHelper::class);
    }
}
