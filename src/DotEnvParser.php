<?php

namespace Mykolab\LaravelDockerBuilder;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariable;

class DotEnvParser
{
    /**
     * @param string $envFilePath
     *
     * @return Collection<EnvVariable>
     */
    public function parse(string $envFilePath): Collection
    {
        if (! File::exists($envFilePath)) {
            throw new InvalidArgumentException(
                sprintf('File %s does not exist.', $envFilePath)
            );
        }

        $envLines = explode("\n", File::get($envFilePath));

        $envVariables = collect();
        foreach ($envLines as $envLine) {
            if ($this->shouldSkipLine($envLine)) {
                continue;
            }

            $envVariables->add(
                $this->parseLine($envLine)
            );
        }

        return $envVariables;
    }

    private function parseLine(string $line): EnvVariable
    {
        [$name, $value] = explode('=', $line, 2);

        $value = (string) Str::of($value)
            ->trim(' ')
            ->trim('"');

        return new EnvVariable($name, $value);
    }

    private function shouldSkipLine(string $line): bool
    {
        return empty(trim($line)) || Str::of($line)->startsWith('#');
    }
}
