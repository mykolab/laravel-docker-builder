<?php

namespace Mykolab\LaravelDockerBuilder\Collections;

use Illuminate\Support\Collection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasVolume;

class DockerServiceCollection extends Collection
{
    /**
     * @param array|string[] $dockerServiceNames
     * @return $this
     */
    public function whereNameIn(array $dockerServiceNames): static
    {
        return $this->filter(
            fn (DockerService $dockerService) => in_array($dockerService->getName(), $dockerServiceNames)
        )->values();
    }

    public function containsDockerService(string $dockerServiceName): bool
    {
        return $this->filter(
            fn (DockerService $dockerService) => $dockerService->getName() === $dockerServiceName
        )->isNotEmpty();
    }

    /**
     * @param array|string[] $dockerServiceClassNames
     * @return $this
     */
    public function whereClassNameIn(array $dockerServiceClassNames): static
    {
        return $this->filter(
            fn (DockerService $dockerService) => in_array($dockerService::class, $dockerServiceClassNames)
        )->values();
    }

    public function whereHasEnvConfiguration(): static
    {
        return $this->filter(
            fn(DockerService $dockerService) => $dockerService instanceof HasEnvironmentConfiguration
        )->values();
    }

    public function whereHasVolume(): static
    {
        return $this->filter(
            fn(DockerService $dockerService) => $dockerService instanceof HasVolume
        )->values();
    }
}
