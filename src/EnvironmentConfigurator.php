<?php

namespace Mykolab\LaravelDockerBuilder;

use ArrayIterator;
use Illuminate\Support\Collection;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariable;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;

class EnvironmentConfigurator
{
    /** @var Collection<EnvVariable> */
    private Collection $envVariables;

    /** @var ArrayIterator<EnvVariableValueProvider> $envVariableValueProviderIterator */
    private ArrayIterator $envVariableValueProviderIterator;

    /**
     * @param string $envName
     * @param Collection<Environment> $environments
     * @param Collection<EnvVariableValueProvider> $envVariableValueProviders
     */
    public function __construct(
        private readonly string $envName,
        private readonly Collection $environments,
        private readonly Collection $envVariableValueProviders
    ) {
        $this->envVariableValueProviderIterator = $this->envVariableValueProviders->getIterator();
        $this->envVariables = collect();
    }

    public function getEnvironment(): Environment
    {
        return new Environment($this->envName, $this->envVariables);
    }

    public function addEnvVariable(EnvVariableValueProvider $envVariableValueProvider, $value): void
    {
        $this->envVariables->add(
            new EnvVariable(
                $envVariableValueProvider->getName(),
                $value,
                $envVariableValueProvider->getNote(),
                $envVariableValueProvider->hasEmptyLine()
            )
        );
    }

    public function nextInputableVariableValueProvider(): ?EnvVariableValueProvider
    {
        if (! $this->envVariableValueProviderIterator->valid()) {
            return null;
        }

        /** @var EnvVariableValueProvider $envVariableValueProvider */
        $envVariableValueProvider = $this->envVariableValueProviderIterator->current();
        $this->envVariableValueProviderIterator->next();

        $value = $envVariableValueProvider->resolveDefaultValue($this->envName);
        if ($envVariableValueProvider->isSameForAllEnvironments() && $this->environments->isNotEmpty()) {
            $value = $this->getVariableValueFromInstalledEnvironment($envVariableValueProvider->getName());
        } else if ($envVariableValueProvider->isRequireInput()) {
            return $envVariableValueProvider;
        }

        $this->addEnvVariable($envVariableValueProvider, $value);

        return $this->nextInputableVariableValueProvider();
    }

    private function getVariableValueFromInstalledEnvironment(string $variableName): ?string
    {
        return $this->environments
            ->first()
            ->envVariables
            ->firstWhere('name', $variableName)
            ?->value;
    }
}
