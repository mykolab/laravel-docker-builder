<?php

namespace Mykolab\LaravelDockerBuilder\Factories;

use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;
use Symfony\Component\Yaml\Yaml;

class DockerServiceFactory
{
    private DockerServiceCollection $dockerServices;

    public function __construct(DockerService ...$availableDockerServices) {
        $this->dockerServices = new DockerServiceCollection($availableDockerServices);
    }

    public function getDockerServices(): DockerServiceCollection
    {
        return $this->dockerServices;
    }

    public function getDefaultDockerServices(): DockerServiceCollection
    {
        return $this->dockerServices->whereClassNameIn(
            config('docker-builder.docker_services.default')
        );
    }

    public function getInstalledDockerServices(): DockerServiceCollection
    {
        $dockerComposeYmlPath = DockerAssets::getDockerDestinationPath('docker-compose.yml');
        if (! File::exists($dockerComposeYmlPath)) {
            return new DockerServiceCollection();
        }

        $dockerComposeYml = File::get($dockerComposeYmlPath);
        $dockerComposeConfiguration = Yaml::parse($dockerComposeYml);

        $dockerServiceNames = array_keys(
            $dockerComposeConfiguration['services'] ?? []
        );

        return $this->getDockerServices()->whereNameIn($dockerServiceNames);
    }
}
