<?php

namespace Mykolab\LaravelDockerBuilder\Factories;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DotEnvParser;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\EnvironmentConfigurator;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;
use Symfony\Component\Finder\SplFileInfo;

class EnvironmentFactory
{
    /**
     * @param DockerServiceFactory $dockerServiceFactory
     * @param DotEnvParser $dotEnvParser
     * @param array|EnvironmentConfiguration[] $defaultEnvironmentConfigurations
     */
    public function __construct(
        private readonly DockerServiceFactory $dockerServiceFactory,
        private readonly DotEnvParser $dotEnvParser,
        private readonly array $defaultEnvironmentConfigurations = []
    ) {}

    /**
     * @return Collection<Environment>
     */
    public function getInstalledEnvironments(): Collection
    {
        return collect(
            File::files(DockerAssets::getDockerDestinationPath())
        )->filter(
            fn(SplFileInfo $splFileInfo) => Str::of($splFileInfo->getFilename())->startsWith('.env.')
        )->map(function (SplFileInfo $splFileInfo) {
            $envName = Str::of($splFileInfo->getFilename())->afterLast('.env.');
            $envVariables = $this->dotEnvParser->parse($splFileInfo->getPathname());

            return new Environment($envName, $envVariables);
        });
    }

    /**
     * @return Collection<EnvVariableValueProvider>
     */
    public function getInstalledEnvVariableValueProviders(): Collection
    {
        $defaultEnvVariableProviders = collect($this->defaultEnvironmentConfigurations)
            ->map(
                fn(EnvironmentConfiguration $environmentConfiguration) => $environmentConfiguration->getValueProviders()
            )
            ->flatten();

        $installedEnvVariableProviders = $this->dockerServiceFactory->getInstalledDockerServices()
            ->whereHasEnvConfiguration()
            ->map(
                fn(DockerService|HasEnvironmentConfiguration $dockerService) => $dockerService->getEnvironmentConfiguration()->getValueProviders()
            )
            ->flatten()
            ->toBase();

        return $defaultEnvVariableProviders->merge($installedEnvVariableProviders);
    }

    public function newEnvironmentConfigurator(string $envName): EnvironmentConfigurator
    {
        return new EnvironmentConfigurator(
            $envName,
            $this->getInstalledEnvironments(),
            $this->getInstalledEnvVariableValueProviders()
        );
    }
}
