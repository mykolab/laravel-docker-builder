<?php

namespace Mykolab\LaravelDockerBuilder;

use Illuminate\Support\Str;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariable;

class EnvFileContentBuilder
{
    public function build(Environment $environment): string
    {
        $envContent = 'ENV_NAME='.$environment->name."\n";

        /** @var EnvVariable $envVariable */
        foreach ($environment->envVariables as $envVariable) {
            $name = $envVariable->name;
            $value = $this->getFormattedVariableValue($envVariable->value);

            if ($envVariable->note) {
                $envContent .= $envVariable->note;
            }

            $envContent .= $name.'='.$value."\n";
            if ($envVariable->hasEmptyLine) {
                $envContent .= "\n";
            }
        }

        return $envContent;
    }

    private function getFormattedVariableValue(float|bool|int|string|null $value): string
    {
        if (is_null($value)) {
            return 'null';
        }

        if (is_bool($value)) {
            return $value ? 'true' : 'false';
        }

        if (Str::of($value)->contains('#') || Str::of($value)->contains(' ') || Str::of($value)->test('/\$\{\w+\}/')) {
            return '"'.$value.'"';
        }

        return $value;
    }
}
