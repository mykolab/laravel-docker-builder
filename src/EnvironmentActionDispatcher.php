<?php

namespace Mykolab\LaravelDockerBuilder;

use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\HasAfterEnvironmentCreatedAction;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;

class EnvironmentActionDispatcher
{
    public function __construct(
        private readonly DockerServiceFactory $dockerServiceFactory
    ) {}

    public function dispatchAfterEnvironmentCreatedAction(Environment $environment): void
    {
        $this->dockerServiceFactory->getInstalledDockerServices()
            ->whereHasEnvConfiguration()
            ->toBase()
            ->map(
                fn(DockerService|HasEnvironmentConfiguration $dockerService) => $dockerService->getEnvironmentConfiguration()
            )
            ->filter(
                fn(EnvironmentConfiguration $envConfiguration) => $envConfiguration instanceof HasAfterEnvironmentCreatedAction
            )
            ->each(
                fn(EnvironmentConfiguration|HasAfterEnvironmentCreatedAction $envConfiguration) => $envConfiguration->afterEnvironmentCreated($environment)
            );
    }
}
