<?php

namespace Mykolab\LaravelDockerBuilder\DTO;

use Closure;
use Illuminate\Support\Str;
use function trans;

class EnvVariableValueProvider
{
    private Closure|string|int|float|bool|null $defaultValue = null;
    private bool $isRequireInput = false;
    private string $description = '';
    private string $note = '';
    private array $choices = [];
    private bool $isSameForAllEnvironments = true;
    private bool $hasEmptyLine = false;

    public function __construct(
        private readonly string $name
    ) {
        $this
            ->description($this->resolveDescriptionFromTranslationFile())
            ->note($this->resolveNoteFromTranslationFile());
    }

    public static function make(string $name): static
    {
        return new static($name);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function defaultValue(Closure|string|int|float|bool|null $defaultValue): static
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    public function resolveDefaultValue(string $envName): string|int|float|bool|null
    {
        if ($this->defaultValue instanceof Closure) {
            return call_user_func($this->defaultValue, $envName);
        }

        return $this->defaultValue;
    }

    public function requireInput(): static
    {
        $this->isRequireInput = true;

        return $this;
    }

    public function isRequireInput(): bool
    {
        return $this->isRequireInput;
    }

    public function description(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    private function resolveDescriptionFromTranslationFile(): string
    {
        $translationKey = 'docker-builder::env_configuration.variables.description.'.$this->name;
        if (! trans()->has($translationKey)) {
            return '';
        }

        return trans($translationKey);
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function note(string $note): static
    {
        $this->note = $note;
        if (!empty($note)) {
            $this->note = '# '.$this->note."\n";
        }

        return $this;
    }

    public function emptyLine(): static
    {
        $this->hasEmptyLine = true;

        return $this;
    }

    public function hasEmptyLine(): bool
    {
        return $this->hasEmptyLine;
    }

    private function resolveNoteFromTranslationFile(): string
    {
        $translationKey = 'docker-builder::env_configuration.variables.note.'.$this->name;
        if (! trans()->has($translationKey)) {
            return '';
        }

        return trans($translationKey);
    }

    public function getNote(): string
    {
        return $this->note;
    }

    public function choices(array $choices): static
    {
        $this->choices = $choices;

        return $this;
    }

    public function hasChoices(): bool
    {
        return count($this->choices) > 0;
    }

    public function getChoices(): array
    {
        return $this->choices;
    }

    public function differentForAllEnvironments(): static
    {
        $this->isSameForAllEnvironments = false;

        return $this;
    }

    public function isSameForAllEnvironments(): bool
    {
        return $this->isSameForAllEnvironments;
    }
}
