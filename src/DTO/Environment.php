<?php

namespace Mykolab\LaravelDockerBuilder\DTO;

use Illuminate\Support\Collection;

class Environment
{
    /**
     * @param string $name
     * @param Collection<EnvVariable> $envVariables
     */
    public function __construct(
        public readonly string $name,
        public readonly Collection $envVariables
    ) {}
}
