<?php

namespace Mykolab\LaravelDockerBuilder\DTO;

class EnvVariable
{
    public function __construct(
        public readonly string $name,
        public readonly string|int|float|bool|null $value,
        public readonly string $note = '',
        public readonly bool $hasEmptyLine = false
    ) {}
}
