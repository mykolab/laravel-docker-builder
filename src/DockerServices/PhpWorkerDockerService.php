<?php

namespace Mykolab\LaravelDockerBuilder\DockerServices;

use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;

class PhpWorkerDockerService implements DockerService
{
    private string $name = 'php-worker';

    public function getName(): string
    {
        return $this->name;
    }
}
