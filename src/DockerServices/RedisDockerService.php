<?php

namespace Mykolab\LaravelDockerBuilder\DockerServices;

use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\EnvironmentConfigurations\RedisEnvironmentConfiguration;

class RedisDockerService implements DockerService, HasEnvironmentConfiguration
{
    private string $name = 'redis';

    public function __construct(
        private readonly RedisEnvironmentConfiguration $environmentConfiguration
    ) {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getEnvironmentConfiguration(): EnvironmentConfiguration
    {
        return $this->environmentConfiguration;
    }
}
