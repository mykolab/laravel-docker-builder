<?php

namespace Mykolab\LaravelDockerBuilder\DockerServices;

use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\EnvironmentConfigurations\WorkspaceEnvironmentConfiguration;

class WorkspaceDockerService implements DockerService, HasEnvironmentConfiguration
{
    private string $name = 'workspace';

    public function __construct(
        private readonly WorkspaceEnvironmentConfiguration $environmentConfiguration
    ) {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getEnvironmentConfiguration(): EnvironmentConfiguration
    {
        return $this->environmentConfiguration;
    }
}
