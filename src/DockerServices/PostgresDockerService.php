<?php

namespace Mykolab\LaravelDockerBuilder\DockerServices;

use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasAfterDockerServiceInstalledAction;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasVolume;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\EnvironmentConfigurations\PostgresEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;

class PostgresDockerService implements DockerService, HasEnvironmentConfiguration, HasVolume, HasAfterDockerServiceInstalledAction
{
    private string $name = 'postgres';

    public function __construct(
        private readonly PostgresEnvironmentConfiguration $environmentConfiguration
    ) {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getEnvironmentConfiguration(): EnvironmentConfiguration
    {
        return $this->environmentConfiguration;
    }

    public function getVolumeOptions(): array
    {
        return [
            'driver' => 'local',
        ];
    }

    public function afterDockerServiceInstalled(): void
    {
        $entryPointDir = DockerAssets::getDockerDestinationPath($this->getName().'/docker-entrypoint-initdb.d/init_test_db.sh');

        File::chmod($entryPointDir, 0755);
    }
}
