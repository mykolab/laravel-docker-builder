<?php

namespace Mykolab\LaravelDockerBuilder\DockerServices;

use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\EnvironmentConfigurations\PhpFpmEnvironmentConfiguration;

class PhpFpmDockerService implements DockerService, HasEnvironmentConfiguration
{
    private string $name = 'php-fpm';

    public function __construct(
        private readonly PhpFpmEnvironmentConfiguration $environmentConfiguration
    ) {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getEnvironmentConfiguration(): EnvironmentConfiguration
    {
        return $this->environmentConfiguration;
    }
}
