<?php

namespace Mykolab\LaravelDockerBuilder\DockerServices;

use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\EnvironmentConfigurations\NginxEnvironmentConfiguration;

class NginxDockerService implements DockerService, HasEnvironmentConfiguration
{
    private string $name = 'nginx';

    public function __construct(
        private readonly NginxEnvironmentConfiguration $environmentConfiguration
    ) {}

    public function getName(): string
    {
        return $this->name;
    }

    public function getEnvironmentConfiguration(): EnvironmentConfiguration
    {
        return $this->environmentConfiguration;
    }
}
