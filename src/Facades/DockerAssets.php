<?php

namespace Mykolab\LaravelDockerBuilder\Facades;

use Illuminate\Support\Facades\Facade;
use Mykolab\LaravelDockerBuilder\Support\DockerAssetsHelper;

/**
 * @method static string getDockerDestinationPath(?string $relativePath = null)
 * @method static string getDockerResourcesPath(?string $relativePath = null)
 * @method static string getDockerComposeYmlStubsPath(string $stubName)
 *
 * @see DockerAssetsHelper
 */
class DockerAssets extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'docker-assets-helper';
    }
}
