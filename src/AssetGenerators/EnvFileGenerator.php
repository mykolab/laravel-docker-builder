<?php

namespace Mykolab\LaravelDockerBuilder\AssetGenerators;

use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\EnvFileContentBuilder;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;

class EnvFileGenerator
{
    public function __construct(
        private readonly EnvFileContentBuilder $envFileContentBuilder
    ) {}

    public function generate(Environment $environment): bool
    {
        $envFileName = '.env.'.$environment->name;
        $envContent = $this->envFileContentBuilder->build($environment);

        return (bool) File::put(DockerAssets::getDockerDestinationPath($envFileName), $envContent);
    }
}
