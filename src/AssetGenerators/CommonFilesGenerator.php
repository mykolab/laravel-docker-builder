<?php

namespace Mykolab\LaravelDockerBuilder\AssetGenerators;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;
use Symfony\Component\Finder\SplFileInfo;

class CommonFilesGenerator
{
    public function __construct(
        private readonly DockerServiceFactory $dockerServiceFactory
    ) {}

    public function generate(): void
    {
        $excludedFiles = $this->getExcludedFiles();

        collect(File::files(DockerAssets::getDockerResourcesPath(), true))
            ->reject(
                fn(SplFileInfo $fileInfo) => $excludedFiles->contains($fileInfo->getBasename())
            )
            ->each(
                fn(SplFileInfo $fileInfo) => File::copy(
                    $fileInfo->getPathname(),
                    DockerAssets::getDockerDestinationPath($fileInfo->getBasename())
                )
            );
    }

    private function getExcludedFiles(): Collection
    {
        $certbotInstalled =$this->dockerServiceFactory
            ->getInstalledDockerServices()
            ->containsDockerService('certbot');

        if (! $certbotInstalled) {
            return collect([
                'deploy.sh',
                'init-letsencrypt.sh',
            ]);
        }

        return collect();
    }
}
