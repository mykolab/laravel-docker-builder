<?php

namespace Mykolab\LaravelDockerBuilder\AssetGenerators;

use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasAfterDockerServiceInstalledAction;
use Mykolab\LaravelDockerBuilder\Exceptions\DockerBuilderException;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;
use Mykolab\LaravelDockerBuilder\StubReplacers\DockerFileStubReplacer;

class DockerFilesGenerator
{
    public function __construct(
        private readonly DockerFileStubReplacer $dockerFileStubReplacer
    ) {}

    /**
     * @throws DockerBuilderException
     */
    public function generate(DockerServiceCollection $dockerServices): void
    {
        $destinationPath = DockerAssets::getDockerDestinationPath();

        if (File::exists($destinationPath)) {
            throw new DockerBuilderException('Docker configuration already exists.');
        }

        File::makeDirectory(DockerAssets::getDockerDestinationPath());
        $this->copyDockerResourcesToDestinationFolder($dockerServices);
        $this->generateDockerComposeFile($dockerServices);
    }

    private function copyDockerResourcesToDestinationFolder(DockerServiceCollection $dockerServices): void
    {
        foreach ($dockerServices as $dockerService) {
            $resourcePath = DockerAssets::getDockerResourcesPath($dockerService->getName());
            $destinationPath = DockerAssets::getDockerDestinationPath($dockerService->getName());

            if (File::exists($resourcePath) && File::copyDirectory($resourcePath, $destinationPath)) {
                if ($dockerService instanceof HasAfterDockerServiceInstalledAction) {
                    $dockerService->afterDockerServiceInstalled();
                }
            }
        }
    }

    private function generateDockerComposeFile(DockerServiceCollection $dockerServices): void
    {
        $dockerComposeStubContent = File::get(
            DockerAssets::getDockerComposeYmlStubsPath('docker-compose')
        );

        $dockerComposeYmlContent = $this->dockerFileStubReplacer->replace(
            $dockerComposeStubContent,
            $dockerServices
        );

        File::put(
            DockerAssets::getDockerDestinationPath('docker-compose.yml'),
            $dockerComposeYmlContent
        );
    }
}
