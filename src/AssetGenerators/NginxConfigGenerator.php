<?php

namespace Mykolab\LaravelDockerBuilder\AssetGenerators;

use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;
use Mykolab\LaravelDockerBuilder\StubReplacers\NginxConfigStubReplacer;

class NginxConfigGenerator
{
    private string $destinationPath;

    private string $stubsPath;

    public function __construct(
        private readonly NginxConfigStubReplacer $nginxConfigStubReplacer
    ) {
        $this->destinationPath = DockerAssets::getDockerDestinationPath('nginx/site-templates');
        $this->stubsPath = DockerAssets::getDockerResourcesPath('nginx/site-stubs');
    }

    public function createNginxConfigFromStub(string $envName, array $domains, string $configStubFileName): void
    {
        $stubContent = File::get($this->stubsPath.'/'.$configStubFileName);

        $nginxConfigContent = $this->nginxConfigStubReplacer->replace($stubContent, $envName, $domains);

        $configDestinationFolderPath = $this->destinationPath.'/'.$envName.'/';
        File::ensureDirectoryExists($configDestinationFolderPath);

        $configDestinationPath = $configDestinationFolderPath.$configStubFileName;
        File::put($configDestinationPath, $nginxConfigContent);
    }
}
