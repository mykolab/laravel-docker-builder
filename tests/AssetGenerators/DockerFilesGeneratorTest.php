<?php

namespace Mykolab\LaravelDockerBuilder\Tests\AssetGenerators;

use Illuminate\Support\Facades\File;
use Mockery;
use Mykolab\LaravelDockerBuilder\AssetGenerators\DockerFilesGenerator;
use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasAfterDockerServiceInstalledAction;
use Mykolab\LaravelDockerBuilder\Exceptions\DockerBuilderException;
use Mykolab\LaravelDockerBuilder\StubReplacers\DockerFileStubReplacer;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class DockerFilesGeneratorTest extends TestCase
{
    /** @test */
    public function it_throws_exception_when_target_docker_configuration_already_exists(): void
    {
        File::partialMock()
            ->shouldReceive('exists')
            ->andReturnTrue();

        $this->expectException(DockerBuilderException::class);

        $dockerServices = new DockerServiceCollection();

        $dockerFilesGenerator = new DockerFilesGenerator(new DockerFileStubReplacer());
        $dockerFilesGenerator->generate($dockerServices);
    }

    /** @test */
    public function it_copy_docker_resources_according_to_selected_docker_services(): void
    {
        $dockerServices = $this->createDockerServicesMock('foo', 'bar');

        File::partialMock()
            ->shouldReceive([
                'get' => 'dummy docker file yml stub content',
                'put' => 1,
                'makeDirectory' => true,
            ]);

        File::partialMock()
            ->shouldReceive('exists')
            ->andReturnValues([false, true]);

        File::partialMock()
            ->shouldReceive('copyDirectory')
            ->times($dockerServices->count());

        $dockerFilesGenerator = new DockerFilesGenerator(new DockerFileStubReplacer());
        $dockerFilesGenerator->generate($dockerServices);
    }

    /** @test */
    public function it_skip_docker_resources_which_does_not_exists(): void
    {
        $dockerServices = $this->createDockerServicesMock('foo', 'bar', 'baz');

        File::partialMock()
            ->shouldReceive([
                'get' => 'dummy docker file yml stub content',
                'put' => 1,
                'makeDirectory' => true,
            ]);

        File::partialMock()
            ->shouldReceive('exists')
            ->andReturnValues([false, false, true]);

        File::partialMock()
            ->shouldReceive('copyDirectory')
            ->twice(); // skipped first docker resource

        $dockerFilesGenerator = new DockerFilesGenerator(new DockerFileStubReplacer());
        $dockerFilesGenerator->generate($dockerServices);
    }

    /** @test */
    public function it_fires_after_docker_installed_action_when_docker_service_implement_such_contract(): void
    {
        $dockerServiceWithAfterInstalledActionMock = Mockery::mock(DockerService::class, HasAfterDockerServiceInstalledAction::class);
        $dockerServiceWithAfterInstalledActionMock->shouldReceive('getName')->andReturn('baz');
        $dockerServiceWithAfterInstalledActionMock->expects('afterDockerServiceInstalled');

        $dockerServices = $this
            ->createDockerServicesMock('foo', 'bar')
            ->add($dockerServiceWithAfterInstalledActionMock);

        File::partialMock()
            ->shouldReceive([
                'get' => 'dummy docker file yml stub content',
                'put' => 1,
                'makeDirectory' => true,
            ]);

        File::partialMock()
            ->shouldReceive('exists')
            ->andReturnValues([false, true]);

        File::partialMock()
            ->shouldReceive('copyDirectory')
            ->andReturnTrue()
            ->times(3); // skipped first docker resource

        $dockerFilesGenerator = new DockerFilesGenerator(new DockerFileStubReplacer());
        $dockerFilesGenerator->generate($dockerServices);
    }

    /** @test */
    public function it_creates_docker_compose_yml_file(): void
    {
        $dockerServices = $this->createDockerServicesMock('foo', 'bar');

        $dockerFileStubReplacerMock = $this->mock(DockerFileStubReplacer::class);
        $expectedDockerYmlContent = 'dummy docker file yml content';
        $dockerFileStubReplacerMock
            ->shouldReceive('replace')
            ->andReturn($expectedDockerYmlContent);

        File::partialMock()
            ->expects('put')
            ->andReturn(1)
            ->with(
                Mockery::any(),
                $expectedDockerYmlContent
            );
        File::shouldReceive([
            'get' => 'dummy docker file yml stub content',
            'makeDirectory' => true,
            'exists' => false,
        ]);

        $dockerFilesGenerator = new DockerFilesGenerator($dockerFileStubReplacerMock);
        $dockerFilesGenerator->generate($dockerServices);
    }

    protected function createDockerServicesMock(...$dockerServicesNames): DockerServiceCollection
    {
        return new DockerServiceCollection(
            array_map(function (string $dockerServiceName) {
                $dockerServiceMock = $this->mock(DockerService::class);
                $dockerServiceMock->shouldReceive('getName')->andReturn($dockerServiceName);

                return $dockerServiceMock;
            }, $dockerServicesNames)
        );
    }
}
