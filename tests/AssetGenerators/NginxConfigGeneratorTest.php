<?php

namespace Mykolab\LaravelDockerBuilder\Tests\AssetGenerators;

use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\AssetGenerators\NginxConfigGenerator;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;
use Mykolab\LaravelDockerBuilder\StubReplacers\NginxConfigStubReplacer;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class NginxConfigGeneratorTest extends TestCase
{
    /**
     * @test
     * @dataProvider configStubFileNamesProvider
     */
    public function it_generate_nginx_config_file_from_stub(string $configStubFileName): void
    {
        $expectedConfigDestinationFolder = 'path/to/nginx/conf';
        DockerAssets::partialMock()
            ->expects('getDockerDestinationPath')
            ->andReturn($expectedConfigDestinationFolder);

        $nginxConfigStubReplacerMock = $this->mock(NginxConfigStubReplacer::class);
        $expectedNginxConfigContent = 'dummy nginx config';
        $nginxConfigStubReplacerMock->expects('replace')->andReturn($expectedNginxConfigContent);

        $envName = 'local';
        $domains = ['test.site'];

        $expectedConfigDestinationPath = $expectedConfigDestinationFolder."/$envName/$configStubFileName";

        $fileFacadeMock = File::spy();
        $fileFacadeMock->expects('get')->andReturn('dummy nginx config stub');
        $fileFacadeMock->expects('ensureDirectoryExists')->andReturnUndefined();
        $fileFacadeMock
            ->expects('put')
            ->with($expectedConfigDestinationPath, $expectedNginxConfigContent)
            ->andReturn(1);

        $nginxConfigGenerator = new NginxConfigGenerator($nginxConfigStubReplacerMock);
        $nginxConfigGenerator->createNginxConfigFromStub(
            $envName,
            $domains,
            $configStubFileName
        );
    }

    public function configStubFileNamesProvider(): array
    {
        return [
            ['backend.conf.template'],
            ['frontend.conf.template'],
        ];
    }
}
