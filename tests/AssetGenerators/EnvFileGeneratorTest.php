<?php

namespace Mykolab\LaravelDockerBuilder\Tests\AssetGenerators;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Mockery;
use Mykolab\LaravelDockerBuilder\AssetGenerators\EnvFileGenerator;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\EnvFileContentBuilder;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class EnvFileGeneratorTest extends TestCase
{
    /** @test */
    public function it_creates_env_file_with_environment_name_suffix(): void
    {
        $envFileContentBuilderMock = $this->mock(EnvFileContentBuilder::class);
        $expectedEnvContent = 'dummy env content';
        $envFileContentBuilderMock->expects('build')->andReturn($expectedEnvContent);

        $envFileGenerator = new EnvFileGenerator($envFileContentBuilderMock);

        $environmentMock = new Environment('foo', collect());

        File::partialMock()
            ->expects('put')
            ->with(
                Mockery::on(
                    fn(string $destinationPath) => '.env.foo' === Str::afterLast($destinationPath, '/')
                ),
                $expectedEnvContent,
            )
            ->andReturn(1);

        $this->assertTrue($envFileGenerator->generate($environmentMock));
    }
}
