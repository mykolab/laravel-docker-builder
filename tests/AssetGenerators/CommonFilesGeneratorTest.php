<?php

namespace Mykolab\LaravelDockerBuilder\Tests\AssetGenerators;

use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\AssetGenerators\CommonFilesGenerator;
use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class CommonFilesGeneratorTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->emptyTempDirectory();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->emptyTempDirectory();
    }

    /** @test */
    public function it_copy_all_files_from_assets_docker_resources_root_dir_to_target_dir(): void
    {
        $this->createTargetDir();
        $this->createStubsFiles([
            'foo.conf.stub',
            'bar.conf',
            'deploy.sh',
            'init-letsencrypt.sh',
        ]);

        DockerAssets::partialMock()
            ->shouldReceive('getDockerResourcesPath')
            ->once()
            ->andReturn($this->getTempDirPath('stubs'));

        DockerAssets::partialMock()
            ->shouldReceive('getDockerDestinationPath')
            ->times(4)
            ->andReturnUsing(fn ($argument) => $this->getTempDirPath("target/$argument"));


        $dockerServiceFactoryMock = $this->getDockerServiceFactoryMock(hasCertbotService: true);
        $commonFilesGenerator = new CommonFilesGenerator($dockerServiceFactoryMock);

        $commonFilesGenerator->generate();

        $this->assertFileExists($this->getTempDirPath('target/foo.conf.stub'));
        $this->assertFileExists($this->getTempDirPath('target/deploy.sh'));
        $this->assertFileExists($this->getTempDirPath('target/init-letsencrypt.sh'));
    }

    /** @test */
    public function it_exclude_deployment_files_from_copying_when_certbot_is_not_installed(): void
    {
        $this->createTargetDir();
        $this->createStubsFiles([
            'foo.conf.stub',
            'bar.conf',
            'deploy.sh',
            'init-letsencrypt.sh',
        ]);

        DockerAssets::partialMock()
            ->shouldReceive('getDockerResourcesPath')
            ->once()
            ->andReturn($this->getTempDirPath('stubs'));

        DockerAssets::partialMock()
            ->shouldReceive('getDockerDestinationPath')
            ->times(2)
            ->andReturnUsing(fn ($argument) => $this->getTempDirPath("target/$argument"));


        $dockerServiceFactoryMock = $this->getDockerServiceFactoryMock(hasCertbotService: false);
        $commonFilesGenerator = new CommonFilesGenerator($dockerServiceFactoryMock);

        $commonFilesGenerator->generate();

        $this->assertFileExists($this->getTempDirPath('target/foo.conf.stub'));
        $this->assertFileDoesNotExist($this->getTempDirPath('target/deploy.sh'));
        $this->assertFileDoesNotExist($this->getTempDirPath('target/init-letsencrypt.sh'));
    }

    private function createStubsFiles(array $fileNames)
    {
        File::makeDirectory($this->getTempDirPath('stubs'));

        foreach ($fileNames as $fileName) {
            File::put($this->getTempDirPath("stubs/$fileName"), 'dummy content');
        }
    }

    private function createTargetDir()
    {
        File::makeDirectory($this->getTempDirPath('target'));
    }

    private function getDockerServiceFactoryMock(bool $hasCertbotService): DockerServiceFactory
    {
        $dockerServiceCollectionMock = $this->mock(DockerServiceCollection::class);
        $dockerServiceCollectionMock->expects('containsDockerService')->andReturn($hasCertbotService);

        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock
            ->expects('getInstalledDockerServices')
            ->andReturn($dockerServiceCollectionMock);

        return $dockerServiceFactoryMock;
    }
}
