<?php

namespace Mykolab\LaravelDockerBuilder\Tests\Collections;

use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasVolume;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class DockerServiceCollectionTest extends TestCase
{
    /** @test */
    public function it_can_filter_by_docker_service_names(): void
    {
        $dockerServices = new DockerServiceCollection([
            new Foo(),
            new Bar(),
            new Baz(),
            new Xyz(),
        ]);

        $filteredDockerServices = $dockerServices->whereNameIn(['foo', 'bar']);
        $this->assertCount(2, $filteredDockerServices);
    }

    /** @test  */
    public function it_checks_if_docker_service_name_are_contains_in_collection(): void
    {
        $dockerServices = new DockerServiceCollection([
            new Foo(),
            new Bar(),
            new Xyz(),
        ]);

        $this->assertTrue($dockerServices->containsDockerService('bar'));
        $this->assertFalse($dockerServices->containsDockerService('baz'));
    }

    /** @test */
    public function it_can_filter_by_docker_service_class_names(): void
    {
        $dockerServices = new DockerServiceCollection([
            new Foo(),
            new Bar(),
            new Baz(),
            new Xyz(),
        ]);

        $filteredDockerServices = $dockerServices->whereClassNameIn([Foo::class, Baz::class]);
        $this->assertCount(2, $filteredDockerServices);
    }

    /** @test */
    public function it_can_filter_docker_services_which_has_env_configuration(): void
    {
        $dockerServices = new DockerServiceCollection([
            new Foo(),
            new Bar(),
            new Baz(),
            new Xyz(),
        ]);

        $filteredDockerServices = $dockerServices->whereHasEnvConfiguration();
        $this->assertCount(1, $filteredDockerServices);
    }

    /** @test */
    public function it_can_filter_docker_services_which_has_volumes(): void
    {
        $dockerServices = new DockerServiceCollection([
            new Foo(),
            new Bar(),
            new Baz(),
            new Xyz(),
        ]);

        $filteredDockerServices = $dockerServices->whereHasVolume();
        $this->assertCount(2, $filteredDockerServices);
    }
}

class Foo implements DockerService {

    public function getName(): string
    {
        return 'foo';
    }
}

class Bar implements DockerService {

    public function getName(): string
    {
        return 'bar';
    }
}

class Baz implements DockerService, HasVolume, HasEnvironmentConfiguration {

    public function getName(): string
    {
        return 'baz';
    }

    public function getVolumeOptions(): array
    {
        return [
            'test1' => 'test1val',
        ];
    }

    public function getEnvironmentConfiguration(): EnvironmentConfiguration
    {
        return new BazEnvConfig();
    }
}

class Xyz implements DockerService, HasVolume {

    public function getName(): string
    {
        return 'xyz';
    }

    public function getVolumeOptions(): array
    {
        return [
            'test2' => 'test2val',
        ];
    }
}

class BazEnvConfig implements EnvironmentConfiguration {

    public function getValueProviders(): array
    {
        return [
            EnvVariableValueProvider::make('test'),
        ];
    }
}
