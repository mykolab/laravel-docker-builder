<?php

namespace Mykolab\LaravelDockerBuilder\Tests\Support;

use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;
use Mykolab\LaravelDockerBuilder\Support\EnvironmentValueHelper;
use Mykolab\LaravelDockerBuilder\Support\ShellCommandsHelper;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class EnvironmentValueHelperTest extends TestCase
{
    /** @test */
    public function it_return_closure_which_generate_random_string_with_clarified_length(): void
    {
        $environmentValueHelper = $this->getEnvironmentValueHelper();

        $passwordClosure = $environmentValueHelper->getPasswordGeneratorClosure(12);
        $password = $passwordClosure('local');

        $this->assertEquals(12, strlen($password));
    }

    /** @test  */
    public function it_should_install_postgres_when_installed_docker_services_contains_postgres_docker_service(): void
    {
        $installedDockerServicesMock = $this->mock(DockerServiceCollection::class);
        $installedDockerServicesMock->expects('containsDockerService')->andReturnTrue();

        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock
            ->expects('getInstalledDockerServices')
            ->andReturn($installedDockerServicesMock);

        $environmentValueHelper = $this->getEnvironmentValueHelper();

        $this->assertTrue($environmentValueHelper->shouldInstallPostgres());
    }

    /** @test  */
    public function it_should_not_install_postgres_when_installed_docker_services_does_not_contains_postgres_docker_service(): void
    {
        $installedDockerServicesMock = $this->mock(DockerServiceCollection::class);
        $installedDockerServicesMock->expects('containsDockerService')->andReturnFalse();

        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock
            ->expects('getInstalledDockerServices')
            ->andReturn($installedDockerServicesMock);

        $environmentValueHelper = $this->getEnvironmentValueHelper();

        $this->assertFalse($environmentValueHelper->shouldInstallPostgres());
    }

    /** @test  */
    public function it_should_install_mysql_when_installed_docker_services_contains_mysql_docker_service(): void
    {
        $installedDockerServicesMock = $this->mock(DockerServiceCollection::class);
        $installedDockerServicesMock->expects('containsDockerService')->andReturnTrue();

        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock
            ->expects('getInstalledDockerServices')
            ->andReturn($installedDockerServicesMock);

        $environmentValueHelper = $this->getEnvironmentValueHelper();

        $this->assertTrue($environmentValueHelper->shouldInstallMySql());
    }

    /** @test  */
    public function it_should_not_install_mysql_when_installed_docker_services_does_not_contains_mysql_docker_service(): void
    {
        $installedDockerServicesMock = $this->mock(DockerServiceCollection::class);
        $installedDockerServicesMock->expects('containsDockerService')->andReturnFalse();

        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock
            ->expects('getInstalledDockerServices')
            ->andReturn($installedDockerServicesMock);

        $environmentValueHelper = $this->getEnvironmentValueHelper();

        $this->assertFalse($environmentValueHelper->shouldInstallMySql());
    }

    /** @test */
    public function it_return_null_for_gitlab_registry_url_when_git_remote_origin_url_is_null(): void
    {
        $shellExecHelperMock = $this->mock(ShellCommandsHelper::class);
        $shellExecHelperMock
            ->shouldReceive('gitRemoteOriginUrl')
            ->once()
            ->andReturnNull();

        $environmentValueHelper = new EnvironmentValueHelper($shellExecHelperMock);

        $this->assertNull($environmentValueHelper->getRegistryUrlFromGit());
    }

    /**
     * @test
     * @dataProvider gitlabRemoteOriginUrlProvider
     */
    public function it_generate_gitlab_registry_url_from_git_remote_origin_url(
        string $gitRemoteOriginUrl,
        string $expectedGitlabRegistryUrl
    ): void
    {
        $shellExecHelperMock = $this->mock(ShellCommandsHelper::class);
        $shellExecHelperMock
            ->shouldReceive('gitRemoteOriginUrl')
            ->once()
            ->andReturn($gitRemoteOriginUrl);

        $environmentValueHelper = new EnvironmentValueHelper($shellExecHelperMock);

        $this->assertEquals($expectedGitlabRegistryUrl, $environmentValueHelper->getRegistryUrlFromGit());
    }

    public function gitlabRemoteOriginUrlProvider(): array
    {
        return [
            ['git@gitlab.com:foo/bar.git', 'registry.gitlab.com/foo/bar'], // ssh remote origin url
            ['https://gitlab.com/foo/bar.git', 'registry.gitlab.com/foo/bar'], // https remote origin url
        ];
    }

    private function getEnvironmentValueHelper(): EnvironmentValueHelper
    {
        return new EnvironmentValueHelper(
            new ShellCommandsHelper()
        );
    }
}
