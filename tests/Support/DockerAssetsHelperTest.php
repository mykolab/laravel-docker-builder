<?php

namespace Mykolab\LaravelDockerBuilder\Tests\Support;

use Mykolab\LaravelDockerBuilder\Support\DockerAssetsHelper;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class DockerAssetsHelperTest extends TestCase
{
    /** @test */
    public function it_returns_docker_resources_path(): void
    {
        $dockerAssetsHelper = new DockerAssetsHelper();
        $dockerResourcesPath = $dockerAssetsHelper->getDockerResourcesPath();

        $this->assertStringEndsWith('assets/docker-resources', $dockerResourcesPath);
    }

    /** @test */
    public function it_returns_specific_docker_resource_path(): void
    {
        $dockerAssetsHelper = new DockerAssetsHelper();
        $dockerResourcesPath = $dockerAssetsHelper->getDockerResourcesPath('foo');

        $this->assertStringEndsWith('assets/docker-resources/foo', $dockerResourcesPath);
    }

    /** @test */
    public function it_docker_resources_folder_is_exists(): void
    {
        $dockerAssetsHelper = new DockerAssetsHelper();
        $dockerResourcesPath = $dockerAssetsHelper->getDockerResourcesPath();

        $this->assertDirectoryExists($dockerResourcesPath);
    }

    /** @test */
    public function it_returns_docker_compose_yml_stub_path(): void
    {
        $dockerAssetsHelper = new DockerAssetsHelper();
        $dockerComposeYmlStubsPath = $dockerAssetsHelper->getDockerComposeYmlStubsPath('foo');

        $this->assertStringEndsWith('assets/service-stubs/foo.yml.stub', $dockerComposeYmlStubsPath);
    }
}
