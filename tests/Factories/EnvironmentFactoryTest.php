<?php

namespace Mykolab\LaravelDockerBuilder\Tests\Factories;

use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\DotEnvParser;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;
use Mykolab\LaravelDockerBuilder\Factories\EnvironmentFactory;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;
use Symfony\Component\Finder\SplFileInfo;

class EnvironmentFactoryTest extends TestCase
{
    /** @test */
    public function it_returns_empty_collection_of_installed_environments_when_target_directory_dont_have_proper_env_files(): void
    {
        $dotEnvParser = $this->mock(DotEnvParser::class);
        $dotEnvParser->shouldReceive('parse')->andReturn(collect());

        $environmentFactory = new EnvironmentFactory(
            $this->mock(DockerServiceFactory::class),
            $dotEnvParser
        );

        $splFileInfoMock = $this->mock(SplFileInfo::class);
        $splFileInfoMock->shouldReceive([
            'getFilename' => 'test.env', // env file with not proper name
            'getPathname' => 'dummy path',
        ]);
        File::partialMock()
            ->expects('files')
            ->andReturn([$splFileInfoMock]);

        $environments = $environmentFactory->getInstalledEnvironments();
        $this->assertTrue($environments->isEmpty());
    }

    /** @test */
    public function it_returns_collection_of_installed_environments_when_target_directory_have_env_files(): void
    {
        $dotEnvParser = $this->mock(DotEnvParser::class);
        $dotEnvParser->shouldReceive('parse')->andReturn(collect());

        $environmentFactory = new EnvironmentFactory(
            $this->mock(DockerServiceFactory::class),
            $dotEnvParser
        );

        $splFileInfoMock1 = $this->mock(SplFileInfo::class);
        $splFileInfoMock1->shouldReceive([
            'getFilename' => '.env.local',
            'getPathname' => 'dummy path 1',
        ]);
        $splFileInfoMock2 = $this->mock(SplFileInfo::class);
        $splFileInfoMock2->shouldReceive([
            'getFilename' => '.env.production',
            'getPathname' => 'dummy path 1',
        ]);

        File::partialMock()
            ->expects('files')
            ->andReturn([$splFileInfoMock1, $splFileInfoMock2]);

        $environments = $environmentFactory->getInstalledEnvironments();
        $this->assertTrue($environments->isNotEmpty());

        $expectedEnvNames = ['local', 'production'];

        $actualEnvNames = $environments->map(
            fn(Environment $environment) => $environment->name
        )->toArray();

        $this->assertSame($expectedEnvNames, $actualEnvNames);
    }

    /** @test */
    public function it_contains_env_variable_providers_from_default_and_installed_environment_configurations(): void
    {
        $defaultEnvConfigurations = [
            new FooEC(),
        ];

        $expectedInstalledDockerServices = [
            new EF_FooDC(),
        ];

        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock
            ->expects('getInstalledDockerServices')
            ->andReturn(new DockerServiceCollection($expectedInstalledDockerServices));

        $environmentFactory = new EnvironmentFactory(
            $dockerServiceFactoryMock,
            $this->mock(DotEnvParser::class),
            $defaultEnvConfigurations
        );

        $actualEnvVariableProviders = $environmentFactory->getInstalledEnvVariableValueProviders()->toArray();

        $defaultEnvVariableProviders = collect($defaultEnvConfigurations)->map(
            fn(EnvironmentConfiguration $ec) => $ec->getValueProviders()
        )->flatten();
        $expectedInstalledEnvVariableProviders = collect($expectedInstalledDockerServices)
            ->map(
                fn(DockerService|HasEnvironmentConfiguration $dc) => $dc->getEnvironmentConfiguration()->getValueProviders()
            )
            ->flatten();
        $expectedEnvVariableProviders = $defaultEnvVariableProviders->merge($expectedInstalledEnvVariableProviders)->toArray();

        $this->assertContainsOnlyInstancesOf(EnvVariableValueProvider::class, $expectedEnvVariableProviders);
        $this->assertEquals($expectedEnvVariableProviders, $actualEnvVariableProviders);
    }
}

class FooEC implements EnvironmentConfiguration
{
    public function getValueProviders(): array
    {
        return [
            EnvVariableValueProvider::make('foo'),
        ];
    }
}

class BarEC implements EnvironmentConfiguration
{
    public function getValueProviders(): array
    {
        return [
            EnvVariableValueProvider::make('bar'),
        ];
    }
}

class EF_FooDC implements DockerService, HasEnvironmentConfiguration {

    public function getName(): string
    {
        return 'foo-dc';
    }

    public function getEnvironmentConfiguration(): EnvironmentConfiguration
    {
        return new BarEC();
    }
}
