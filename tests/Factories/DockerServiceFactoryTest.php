<?php

namespace Mykolab\LaravelDockerBuilder\Tests\Factories;

use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class DockerServiceFactoryTest extends TestCase
{
    /** @test */
    public function returns_available_docker_services(): void
    {
        $availableDockerServices = [
            new FooDS(),
            new BarDS(),
            new BazDS(),
        ];

        $dockerServiceFactory = new DockerServiceFactory(...$availableDockerServices);

        $actualDockerServices = $dockerServiceFactory->getDockerServices();
        $this->assertInstanceOf(DockerServiceCollection::class, $actualDockerServices);
        $this->assertSame($availableDockerServices, $actualDockerServices->toArray());
    }

    /** @test */
    public function returns_default_docker_services(): void
    {
        $availableDockerServices = [
            $foo = new FooDS(),
            $bar = new BarDS(),
            new BazDS(),
        ];

        $expectedDefaultDockerServices = [$foo, $bar];

        config()->set('docker-builder.docker_services.default', array_map(
            fn(DockerService $dockerService) => $dockerService::class,
            $expectedDefaultDockerServices
        ));

        $dockerServiceFactory = new DockerServiceFactory(...$availableDockerServices);

        $actualDefaultDockerServices = $dockerServiceFactory->getDefaultDockerServices();
        $this->assertInstanceOf(DockerServiceCollection::class, $actualDefaultDockerServices);
        $this->assertSame($expectedDefaultDockerServices, $actualDefaultDockerServices->toArray());
    }

    /** @test */
    public function returns_installed_docker_services(): void
    {
        $availableDockerServices = [
            $foo = new FooDS(),
            $bar = new BarDS(),
            new BazDS(),
        ];

        $expectedInstalledDockerServices = [$foo, $bar];

        $dockerServiceFactory = new DockerServiceFactory(...$availableDockerServices);

        DockerAssets::partialMock()
            ->shouldReceive('getDockerDestinationPath')
            ->once()
            ->andReturns($this->getTestFilesPath('docker-compose.yml'));

        $actualInstalledServices = $dockerServiceFactory->getInstalledDockerServices();
        $this->assertInstanceOf(DockerServiceCollection::class, $actualInstalledServices);
        $this->assertSame($expectedInstalledDockerServices, $actualInstalledServices->toArray());
    }

    /** @test */
    public function when_docker_compose_file_not_exist_returns_empty_collection(): void
    {
        $availableDockerServices = [
            new FooDS(),
            new BarDS(),
            new BazDS(),
        ];

        $dockerServiceFactory = new DockerServiceFactory(...$availableDockerServices);

        DockerAssets::partialMock()->shouldReceive('getDockerDestinationPath')
            ->once()
            ->andReturns($this->getTestFilesPath('not-existing.yml'));

        $this->assertTrue($dockerServiceFactory->getInstalledDockerServices()->isEmpty());
    }
}

class FooDS implements DockerService
{
    public function getName(): string
    {
        return 'foo';
    }
}

class BarDS implements DockerService
{
    public function getName(): string
    {
        return 'bar';
    }
}

class BazDS implements DockerService
{
    public function getName(): string
    {
        return 'baz';
    }
}
