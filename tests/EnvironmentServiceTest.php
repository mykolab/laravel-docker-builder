<?php

namespace Mykolab\LaravelDockerBuilder\Tests;

use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\EnvironmentService;
use Mykolab\LaravelDockerBuilder\Factories\EnvironmentFactory;

class EnvironmentServiceTest extends TestCase
{
    /** @test */
    public function it_shows_that_env_exists_when_given_env_name_was_already_installed(): void
    {
        $installedEnvironments = collect([
            new Environment('foo', collect())
        ]);

        $environmentFactoryMock = $this->mock(EnvironmentFactory::class);
        $environmentFactoryMock
            ->expects('getInstalledEnvironments')
            ->andReturn($installedEnvironments);

        $environmentService = new EnvironmentService($environmentFactoryMock);
        $this->assertTrue(
            $environmentService->isEnvironmentExists('foo')
        );
    }

    /** @test */
    public function it_shows_that_env_exists_when_given_env_name_was_not_installed(): void
    {
        $installedEnvironments = collect([
            new Environment('foo', collect())
        ]);

        $environmentFactoryMock = $this->mock(EnvironmentFactory::class);
        $environmentFactoryMock
            ->expects('getInstalledEnvironments')
            ->andReturn($installedEnvironments);

        $environmentService = new EnvironmentService($environmentFactoryMock);
        $this->assertFalse(
            $environmentService->isEnvironmentExists('bar')
        );
    }
}
