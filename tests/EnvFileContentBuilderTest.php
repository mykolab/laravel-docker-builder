<?php

namespace Mykolab\LaravelDockerBuilder\Tests;

use Illuminate\Support\Str;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariable;
use Mykolab\LaravelDockerBuilder\EnvFileContentBuilder;

class EnvFileContentBuilderTest extends TestCase
{
    /** @test */
    public function it_adding_env_name_variable_as_first_variable_of_env_file(): void
    {
        $envFileContentBuilder =  new EnvFileContentBuilder();
        $envContent = $envFileContentBuilder->build(
            new Environment('foo', collect())
        );

        $this->assertStringStartsWith('ENV_NAME=foo', $envContent);
    }

    /** @test */
    public function it_splits_env_variable_with_new_line(): void
    {
        $envVariables = collect([
            new EnvVariable('FOO', 'bar'),
            new EnvVariable('BAZ', 'xyz'),
            new EnvVariable('BAZ', 'abc'),
        ]);

        $envFileContentBuilder =  new EnvFileContentBuilder();
        $envContent = $envFileContentBuilder->build(
            new Environment('foo', $envVariables)
        );

        $expectedCount = count($envVariables) + 1; // should match number variables plus default variable ENV_NAME

        $this->assertEquals($expectedCount, Str::substrCount($envContent, "\n"));
    }

    /** @test */
    public function it_convert_null_value_into_null_string(): void
    {
        $envVariables = collect([
            new EnvVariable('FOO', null),
        ]);

        $envFileContentBuilder =  new EnvFileContentBuilder();
        $envContent = $envFileContentBuilder->build(
            new Environment('foo', $envVariables)
        );

        $this->assertStringContainsString('FOO=null', $envContent);
    }

    /** @test */
    public function it_convert_bool_value_into_string(): void
    {
        $envVariables = collect([
            new EnvVariable('FOO', true),
            new EnvVariable('BAR', false),
        ]);

        $envFileContentBuilder =  new EnvFileContentBuilder();
        $envContent = $envFileContentBuilder->build(
            new Environment('foo', $envVariables)
        );

        $this->assertStringContainsString('FOO=true', $envContent);
        $this->assertStringContainsString('BAR=false', $envContent);
    }

    /** @test */
    public function it_wrap_variable_value_into_quotes_when_it_contains_few_words(): void
    {
        $envVariables = collect([
            new EnvVariable('FOO', 'bar baz'),
        ]);

        $envFileContentBuilder =  new EnvFileContentBuilder();
        $envContent = $envFileContentBuilder->build(
            new Environment('foo', $envVariables)
        );

        $this->assertStringContainsString('FOO="bar baz"', $envContent);
    }

    /** @test */
    public function it_wrap_variable_value_into_quotes_when_it_contains_another_env_variable(): void
    {
        $envVariables = collect([
            new EnvVariable('FOO', 'bar ${BAZ}'),
            new EnvVariable('BAR', '${BAZ}'),
        ]);

        $envFileContentBuilder =  new EnvFileContentBuilder();
        $envContent = $envFileContentBuilder->build(
            new Environment('foo', $envVariables)
        );

        $this->assertStringContainsString('FOO="bar ${BAZ}"', $envContent);
        $this->assertStringContainsString('BAR="${BAZ}"', $envContent);
    }

    /** @test */
    public function it_wrap_variable_value_into_quotes_when_it_contains_sharp_sign_in_value(): void
    {
        $envVariables = collect([
            new EnvVariable('FOO', 'xyz#'),
            new EnvVariable('BAR', 'xyz#abc'),
        ]);

        $envFileContentBuilder =  new EnvFileContentBuilder();
        $envContent = $envFileContentBuilder->build(
            new Environment('foo', $envVariables)
        );

        $this->assertStringContainsString('FOO="xyz#"', $envContent);
        $this->assertStringContainsString('BAR="xyz#abc"', $envContent);
    }

    /** @test */
    public function it_does_not_wrap_variable_value_into_quotes_strings_and_numbers(): void
    {
        $envVariables = collect([
            new EnvVariable('FOO', 2),
            new EnvVariable('BAR', 2.3),
            new EnvVariable('BAZ', 'xyz'),
        ]);

        $envFileContentBuilder =  new EnvFileContentBuilder();
        $envContent = $envFileContentBuilder->build(
            new Environment('foo', $envVariables)
        );

        $this->assertStringContainsString('FOO=2', $envContent);
        $this->assertStringContainsString('BAR=2.3', $envContent);
        $this->assertStringContainsString('BAZ=xyz', $envContent);
    }

    /** @test */
    public function it_adding_note_before_variable(): void
    {
        $envVariables = collect([
            new EnvVariable('FOO', 'bar', "# dummy note\n"),
        ]);

        $envFileContentBuilder =  new EnvFileContentBuilder();
        $envContent = $envFileContentBuilder->build(
            new Environment('foo', $envVariables)
        );

        $this->assertStringContainsString("# dummy note\nFOO=bar", $envContent);
    }

    /**
     * @test
     * @dataProvider envVariableEmptyLineProvider
     */
    public function it_adding_empty_line_after_variable_value(EnvVariable $envVariable): void
    {
        $envVariables = collect([
            $envVariable,
        ]);

        $envFileContentBuilder =  new EnvFileContentBuilder();
        $envContent = $envFileContentBuilder->build(
            new Environment('foo', $envVariables)
        );

        $this->assertStringEndsWith("\n\n", $envContent);
    }

    public function envVariableEmptyLineProvider(): array
    {
        return [
            [new EnvVariable(name: 'FOO', value: 'bar', note: "# dummy note\n", hasEmptyLine: true)],
            [new EnvVariable(name: 'FOO', value: 'bar', hasEmptyLine: true)],
        ];
    }
}
