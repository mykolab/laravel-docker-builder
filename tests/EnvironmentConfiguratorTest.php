<?php

namespace Mykolab\LaravelDockerBuilder\Tests;

use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariable;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\EnvironmentConfigurator;

class EnvironmentConfiguratorTest extends TestCase
{
    /** @test */
    public function it_returns_environment_with_env_name_which_was_passed_into_configurator(): void
    {
        $environmentConfigurator = new EnvironmentConfigurator(
            'foo',
            collect(),
            collect(),
        );

        $this->assertEquals('foo', $environmentConfigurator->getEnvironment()->name);
    }

    /** @test */
    public function it_returns_environment_with_empty_env_variables_when_no_env_variable_was_added(): void
    {
        $environmentConfigurator = new EnvironmentConfigurator(
            'foo',
            collect(),
            collect(),
        );

        $this->assertTrue($environmentConfigurator->getEnvironment()->envVariables->isEmpty());
    }

    /** @test */
    public function it_adds_env_variables_to_new_environment(): void
    {
        $environmentConfigurator = new EnvironmentConfigurator(
            'foo',
            collect(),
            collect(),
        );

        $environmentConfigurator->addEnvVariable(EnvVariableValueProvider::make('bar'), 2);
        $environmentConfigurator->addEnvVariable(EnvVariableValueProvider::make('baz'), 'xyz');

        $envVariables = $environmentConfigurator->getEnvironment()->envVariables;

        $this->assertCount(2, $envVariables);
    }

    /** @test */
    public function it_adds_env_variables_with_attributes_copied_from_env_variable_value_provider(): void
    {
        $environmentConfigurator = new EnvironmentConfigurator(
            'foo',
            collect(),
            collect(),
        );

        $envVariableValueProvider = $this->mock(EnvVariableValueProvider::class);
        $envVariableValueProvider
            ->shouldReceive([
                'getName' => 'bar',
                'getNote' => 'xyz',
                'hasEmptyLine' => false,
            ])
            ->once();

        $environmentConfigurator->addEnvVariable($envVariableValueProvider, 'baz');

        /** @var EnvVariable $envVariable */
        $envVariable = $environmentConfigurator->getEnvironment()->envVariables->first();

        $this->assertEquals('baz', $envVariable->value);
        $this->assertEquals('bar', $envVariable->name);
        $this->assertEquals('xyz', $envVariable->note);
        $this->assertEquals(false, $envVariable->hasEmptyLine);
    }

    /** @test */
    public function it_returns_null_for_next_inputable_variable_provider_when_variable_providers_collection_is_empty(): void
    {
        $environmentConfigurator = new EnvironmentConfigurator(
            'foo',
            collect(),
            collect(),
        );

        $this->assertNull($environmentConfigurator->nextInputableVariableValueProvider());
    }

    /** @test */
    public function it_returns_current_variable_provider_when_it_require_input(): void
    {
        $envVariableValueProvider = $this->mock(EnvVariableValueProvider::class);
        $envVariableValueProvider
            ->shouldReceive([
                'resolveDefaultValue' => 'xyz',
                'isSameForAllEnvironments' => false,
                'isRequireInput' => true,
            ])
            ->once();

        $environmentConfigurator = new EnvironmentConfigurator(
            'foo',
            collect(),
            collect([$envVariableValueProvider]),
        );

        $nextInputableVariableValueProvider = $environmentConfigurator->nextInputableVariableValueProvider();

        $this->assertSame($envVariableValueProvider, $nextInputableVariableValueProvider);
    }

    /** @test */
    public function it_copies_env_variable_from_existing_environment(): void
    {
        $envVariableValueProviderMock = $this->mock(EnvVariableValueProvider::class);
        $envVariableName = 'baz';
        $envVariableValueProviderMock->shouldReceive('getName')->twice()->andReturn($envVariableName);
        $envVariableValueProviderMock
            ->shouldReceive([
                'getNote' => 'dummy note',
                'hasEmptyLine' => false,
                'resolveDefaultValue' => 'xyz',
                'isSameForAllEnvironments' => true,
            ])
            ->once();

        $environmentMock = new Environment('env-foo', collect([new EnvVariable($envVariableName, 'test')]));

        $environmentConfigurator = new EnvironmentConfigurator(
            'foo',
            collect([$environmentMock]),
            collect([$envVariableValueProviderMock]),
        );

        $environmentConfigurator->nextInputableVariableValueProvider();

        /** @var EnvVariable $envVariable */
        $envVariable = $environmentConfigurator->getEnvironment()->envVariables->first();

        $this->assertSame($envVariableName, $envVariable->name);
        $this->assertSame('test', $envVariable->value);
    }

    /** @test */
    public function it_use_default_value_when_no_existing_environments_available(): void
    {
        $envVariableValueProviderMock = $this->mock(EnvVariableValueProvider::class);
        $envVariableName = 'baz';
        $envVariableValueProviderMock
            ->shouldReceive([
                'getName' => $envVariableName,
                'getNote' => 'dummy note',
                'hasEmptyLine' => false,
                'resolveDefaultValue' => 'xyz',
                'isSameForAllEnvironments' => true,
                'isRequireInput' => false,
            ])
            ->once();

        $environmentConfigurator = new EnvironmentConfigurator(
            'foo',
            collect(),
            collect([$envVariableValueProviderMock]),
        );

        $environmentConfigurator->nextInputableVariableValueProvider();

        /** @var EnvVariable $envVariable */
        $envVariable = $environmentConfigurator->getEnvironment()->envVariables->first();

        $this->assertSame($envVariableName, $envVariable->name);
        $this->assertSame('xyz', $envVariable->value);
    }

    /** @test */
    public function it_use_default_value_when_input_is_not_required_and_value_is_not_same_for_environments(): void
    {
        $envVariableValueProviderMock1 = $this->getEnvVariableProviderMockForDefaultBehaviour('bar', 'test');
        $envVariableValueProviderMock2 = $this->getEnvVariableProviderMockForDefaultBehaviour('baz', 2);

        $environmentConfigurator = new EnvironmentConfigurator(
            'foo',
            collect(),
            collect([$envVariableValueProviderMock1, $envVariableValueProviderMock2]),
        );

        $environmentConfigurator->nextInputableVariableValueProvider(); // get first variable value provider
        $environmentConfigurator->nextInputableVariableValueProvider(); // get second variable value provider

        $envVariables = $environmentConfigurator->getEnvironment()->envVariables;

        $this->assertCount(2, $envVariables);

        $envVariable1 = $envVariables->get(0);
        $envVariable2 = $envVariables->get(1);

        $this->assertEquals('bar', $envVariable1->name);
        $this->assertEquals('test', $envVariable1->value);

        $this->assertEquals('baz', $envVariable2->name);
        $this->assertEquals(2, $envVariable2->value);
    }

    /** @test */
    public function it_returns_null_on_last_iteration(): void
    {
        $envVariableValueProviderMock1 = $this->getEnvVariableProviderMockForRequiredInput('bar', 'test');
        $envVariableValueProviderMock2 = $this->getEnvVariableProviderMockForRequiredInput('baz', 2);
        $envVariableValueProviderMock3 = $this->getEnvVariableProviderMockForRequiredInput('xyz', true);

        $environmentConfigurator = new EnvironmentConfigurator(
            'foo',
            collect(),
            collect([$envVariableValueProviderMock1, $envVariableValueProviderMock2, $envVariableValueProviderMock3]),
        );

        $this->assertNotNull($environmentConfigurator->nextInputableVariableValueProvider());
        $this->assertNotNull($environmentConfigurator->nextInputableVariableValueProvider());
        $this->assertNotNull($environmentConfigurator->nextInputableVariableValueProvider());
        $this->assertNull($environmentConfigurator->nextInputableVariableValueProvider());
    }

    private function getEnvVariableProviderMockForDefaultBehaviour(
        string $name,
        string|int|float|bool|null $value
    ): EnvVariableValueProvider
    {
        $envVariableValueProviderMock = $this->mock(EnvVariableValueProvider::class);
        $envVariableValueProviderMock
            ->shouldReceive([
                'getName' => $name,
                'getNote' => 'dummy note',
                'hasEmptyLine' => false,
                'resolveDefaultValue' => $value,
                'isSameForAllEnvironments' => false,
                'isRequireInput' => false,
            ])
            ->once();

        return $envVariableValueProviderMock;
    }

    private function getEnvVariableProviderMockForRequiredInput(
        string $name,
        string|int|float|bool|null $value
    ): EnvVariableValueProvider
    {
        $envVariableValueProviderMock = $this->mock(EnvVariableValueProvider::class);
        $envVariableValueProviderMock
            ->shouldReceive([
                'resolveDefaultValue' => $value,
                'isSameForAllEnvironments' => false,
                'isRequireInput' => true,
            ])
            ->once();

        return $envVariableValueProviderMock;
    }
}
