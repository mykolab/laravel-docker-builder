<?php

namespace Mykolab\LaravelDockerBuilder\Tests;

use Illuminate\Support\Facades\File;
use InvalidArgumentException;
use Mykolab\LaravelDockerBuilder\DotEnvParser;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariable;

class DotEnvParserTest extends TestCase
{
    /** @test */
    public function it_throws_exception_when_env_file_not_found(): void
    {
        $dotEnvParser = new DotEnvParser();

        File::partialMock()->expects('exists')->andReturnFalse();

        $this->expectException(InvalidArgumentException::class);

        $dotEnvParser->parse('not-existing-env.test');
    }

    /** @test */
    public function it_returns_env_variables_collection_from_env_file(): void
    {
        $dotEnvParser = new DotEnvParser();

        $envFileContentMock = <<< TEXT
            FOO=bar
            BAZ="test/\${FOO}"

            # comment

            XYZ="q!@#$%"
            TEXT;

        File::partialMock()->expects('exists')->andReturnTrue();
        File::partialMock()->expects('get')->andReturn($envFileContentMock);

        $envVariables = $dotEnvParser->parse($this->getTestFilesPath('.env.test'))->mapWithKeys(
            fn(EnvVariable $envVariable) => [$envVariable->name => $envVariable->value]
        );

        $expected = [
            'FOO' => 'bar',
            'BAZ' => 'test/${FOO}',
            'XYZ' => 'q!@#$%',
        ];
        $this->assertSame($expected, $envVariables->toArray());
    }
}
