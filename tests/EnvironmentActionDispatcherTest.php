<?php

namespace Mykolab\LaravelDockerBuilder\Tests;

use Illuminate\Support\Collection;
use Mockery;
use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasEnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\EnvironmentConfiguration;
use Mykolab\LaravelDockerBuilder\Contracts\EnvironmentConfiguration\HasAfterEnvironmentCreatedAction;
use Mykolab\LaravelDockerBuilder\DTO\Environment;
use Mykolab\LaravelDockerBuilder\EnvironmentActionDispatcher;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;

class EnvironmentActionDispatcherTest extends TestCase
{
    /** @test */
    public function it_dispatches_all_environment_configurations_which_has_implemented_after_environment_created_contract(): void
    {
        $installedDockerServices = $this->getInstalledDockerServicesMock();
        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock->expects('getInstalledDockerServices')->andReturn($installedDockerServices);

        $environmentActionDispatcher = new EnvironmentActionDispatcher($dockerServiceFactoryMock);
        $environmentActionDispatcher->dispatchAfterEnvironmentCreatedAction(
            new Environment('xyz', collect())
        );
    }

    private function getInstalledDockerServicesMock(): Collection
    {
        $fooDS = Mockery::mock(DockerService::class, HasEnvironmentConfiguration::class);
        $fooDS->shouldReceive('getName')->andReturn('foo');
        $fooEnvConfigurationMock = Mockery::mock(EnvironmentConfiguration::class, HasAfterEnvironmentCreatedAction::class);
        $fooEnvConfigurationMock->expects('afterEnvironmentCreated')->andReturnUndefined();
        $fooDS->expects('getEnvironmentConfiguration')->andReturn($fooEnvConfigurationMock);

        $barDS = Mockery::mock(DockerService::class, HasEnvironmentConfiguration::class);
        $barDS->shouldReceive('getName')->andReturn('bar');
        $barEnvConfigurationMock = Mockery::mock(EnvironmentConfiguration::class);
        $barDS->expects('getEnvironmentConfiguration')->andReturn($barEnvConfigurationMock);

        $bazDS = Mockery::mock(DockerService::class);
        $bazDS->shouldReceive('getName')->andReturn('baz');

        $xyzDS = Mockery::mock(DockerService::class, HasEnvironmentConfiguration::class);
        $xyzDS->shouldReceive('getName')->andReturn('xyz');
        $xyzEnvConfigurationMock = Mockery::mock(EnvironmentConfiguration::class, HasAfterEnvironmentCreatedAction::class);
        $xyzEnvConfigurationMock->expects('afterEnvironmentCreated')->andReturnUndefined();
        $xyzDS->expects('getEnvironmentConfiguration')->andReturn($xyzEnvConfigurationMock);

        return new DockerServiceCollection([
            $fooDS,
            $barDS,
            $bazDS,
            $xyzDS,
        ]);
    }
}
