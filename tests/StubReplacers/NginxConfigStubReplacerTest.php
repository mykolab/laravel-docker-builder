<?php

namespace Mykolab\LaravelDockerBuilder\Tests\StubReplacers;

use Closure;
use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;
use Mykolab\LaravelDockerBuilder\StubReplacers\NginxConfigStubReplacer;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class NginxConfigStubReplacerTest extends TestCase
{
    /**
     * @test
     * @dataProvider configStubFileNamesProvider
     */
    public function it_removes_comments_with_pattern_markers_in_config_file(Closure $configStubContentClosure): void
    {
        $siteConfigStubContent = $configStubContentClosure();

        $this->assertStringContainsString('#start_release_configuration', $siteConfigStubContent);
        $this->assertStringContainsString('#end_release_configuration', $siteConfigStubContent);
        $this->assertStringContainsString('#start_local_configuration', $siteConfigStubContent);
        $this->assertStringContainsString('#end_local_configuration', $siteConfigStubContent);

        $envName = 'local';
        $domains = ['test.site'];

        $nginxConfigStubReplacer = new NginxConfigStubReplacer();
        $siteConfigContent = $nginxConfigStubReplacer->replace($siteConfigStubContent, $envName, $domains);

        $this->assertStringNotContainsString('#start_release_configuration', $siteConfigContent);
        $this->assertStringNotContainsString('#end_release_configuration', $siteConfigContent);
        $this->assertStringNotContainsString('#start_local_configuration', $siteConfigContent);
        $this->assertStringNotContainsString('#end_local_configuration', $siteConfigContent);
    }

    /**
     * @test
     * @dataProvider configStubFileNamesProvider
     */
    public function it_removes_release_pattern_content_for_local_environment_in_config_file(
        Closure $configStubContentClosure
    ): void
    {
        $siteConfigStubContent = $configStubContentClosure();

        $this->assertStringContainsString('listen 443 ssl;', $siteConfigStubContent);

        $envName = 'local';
        $domains = ['test.site'];

        $nginxConfigStubReplacer = new NginxConfigStubReplacer();
        $siteConfigContent = $nginxConfigStubReplacer->replace($siteConfigStubContent, $envName, $domains);

        $this->assertStringNotContainsString('listen 443 ssl;', $siteConfigContent);
        $this->assertEquals(
            1,
            substr_count($siteConfigContent, 'server {'),
            'Should have only one "server" configuration (without HTTPS redirects)'
        );
    }

    /**
     * @test
     * @dataProvider configStubFileNamesProvider
     */
    public function it_keeps_release_pattern_content_for_non_local_environment_in_config_file(
        Closure $configStubContentClosure
    ): void
    {
        $siteConfigStubContent = $configStubContentClosure();

        $envName = 'foo';
        $domains = ['test.site'];

        $nginxConfigStubReplacer = new NginxConfigStubReplacer();
        $siteConfigContent = $nginxConfigStubReplacer->replace($siteConfigStubContent, $envName, $domains);

        $this->assertStringContainsString('listen 443 ssl;', $siteConfigContent); // make sure that it
        $this->assertEquals(
            2,
            substr_count($siteConfigContent, 'server {'),
            'Should have two "server" configurations (default and HTTPS redirect)'
        );
    }

    /**
     * @test
     * @dataProvider configStubFileNamesProvider
     */
    public function it_keep_release_pattern_content_in_config_file_for_non_local_environment(
        Closure $configStubContentClosure
    ): void
    {
        $siteConfigStubContent = $configStubContentClosure();

        $this->assertStringContainsString('#start_release_configuration', $siteConfigStubContent);
        $this->assertStringContainsString('#end_release_configuration', $siteConfigStubContent);

        $envName = 'foo';
        $domains = ['test.site'];

        $nginxConfigStubReplacer = new NginxConfigStubReplacer();
        $siteConfigContent = $nginxConfigStubReplacer->replace($siteConfigStubContent, $envName, $domains);

        $this->assertStringNotContainsString('#start_release_configuration', $siteConfigContent);
        $this->assertStringNotContainsString('#end_release_configuration', $siteConfigContent);
        $this->assertStringContainsString('listen 443 ssl;', $siteConfigContent);
        $this->assertEquals(
            2,
            substr_count($siteConfigContent, 'server {'),
            'Should have only two "server" configuration (default and HTTPS redirect)'
        );
    }

    /**
     * @test
     * @dataProvider nginxConfigGeneratorDomainsProvider
     */
    public function it_replaces_domain_templates_to_real_domain_values(
        string $envName,
        array $domains,
        Closure $configStubContentClosure
    ): void
    {
        $siteConfigStubContent = $configStubContentClosure();

        $this->assertStringContainsString('{{domains}}', $siteConfigStubContent);

        $nginxConfigStubReplacer = new NginxConfigStubReplacer();
        $siteConfigContent = $nginxConfigStubReplacer->replace($siteConfigStubContent, $envName, $domains);

        $domainsStr = implode(' ', $domains);

        $this->assertStringContainsString("server_name $domainsStr", $siteConfigContent);
        $this->assertStringNotContainsString('{{domains}}', $siteConfigContent);
    }

    /**
     * @test
     * @dataProvider nginxConfigGeneratorHttpsRedirectsProvider
     */
    public function it_replaces_https_redirects_template_to_real_value(
        string $envName,
        array $domains,
        $configStubContentClosure
    ): void
    {
        $siteConfigStubContent = $configStubContentClosure();

        $this->assertStringContainsString('{{https_redirects}}', $siteConfigStubContent);

        $nginxConfigStubReplacer = new NginxConfigStubReplacer();
        $siteConfigContent = $nginxConfigStubReplacer->replace($siteConfigStubContent, $envName, $domains);

        $httpsRedirectExpectedValue = <<<END
            if (\$host = %s) {
                return 301 https://\$host\$request_uri;
            }
        END;

        $this->assertStringNotContainsString('{{https_redirects}}', $siteConfigContent);
        $this->assertEquals(
            count($domains),
            substr_count($siteConfigContent, 'return 301 https'),
            'HTTPS redirects number should be equal to domains number.'
        );

        foreach ($domains as $domain) {
            $this->assertStringContainsString(
                sprintf($httpsRedirectExpectedValue, $domain),
                $siteConfigContent
            );
        }
    }

    /**
     * @test
     * @dataProvider nginxConfigGeneratorHttpsRedirectsProvider
     */
    public function it_replaces_ssl_domain_template_to_first_domain(
        string $envName,
        array $domains,
        Closure $configStubContentClosure
    ): void
    {
        $siteConfigStubContent = $configStubContentClosure();

        $this->assertStringContainsString('{{ssl_domain}}', $siteConfigStubContent);

        $nginxConfigStubReplacer = new NginxConfigStubReplacer();
        $siteConfigContent = $nginxConfigStubReplacer->replace($siteConfigStubContent, $envName, $domains);

        $sslDomain = $domains[0];

        $this->assertStringNotContainsString('{{ssl_domain}}', $siteConfigContent);
        $this->assertStringContainsString(
            "ssl_certificate /etc/letsencrypt/live/$sslDomain/fullchain.pem",
            $siteConfigContent
        );
        $this->assertStringContainsString(
            "ssl_certificate_key /etc/letsencrypt/live/$sslDomain/privkey.pem",
            $siteConfigContent
        );
    }

    /**
     * @param string $configStubFileName Should be existing nginx config stub.
     * @return string
     */
    private function getConfigStubContentClosure(string $configStubFileName): string
    {
        return File::get(
            DockerAssets::getDockerResourcesPath('/nginx/site-stubs/'.$configStubFileName)
        );
    }

    // ==== Data Providers ====

    public function configStubFileNamesProvider(): array
    {
        return [
            [fn() => $this->getConfigStubContentClosure('backend.conf.template')],
            [fn() => $this->getConfigStubContentClosure('frontend.conf.template')],
        ];
    }

    public function nginxConfigGeneratorDomainsProvider(): array
    {
        return [
            ['local', ['test.site'], fn() => $this->getConfigStubContentClosure('backend.conf.template')],
            ['local', ['test.site'], fn() => $this->getConfigStubContentClosure('frontend.conf.template')],

            ['local', ['test1.site', 'test2.site'], fn() => $this->getConfigStubContentClosure('backend.conf.template')],
            ['local', ['test1.site', 'test2.site'], fn() => $this->getConfigStubContentClosure('frontend.conf.template')],
        ];
    }

    public function nginxConfigGeneratorHttpsRedirectsProvider(): array
    {
        return [
            ['foo', ['test.site'], fn() => $this->getConfigStubContentClosure('backend.conf.template')],
            ['foo', ['test.site'], fn() => $this->getConfigStubContentClosure('frontend.conf.template')],

            ['foo', ['test1.site', 'test2.site'], fn() => $this->getConfigStubContentClosure('backend.conf.template')],
            ['foo', ['test1.site', 'test2.site'], fn() => $this->getConfigStubContentClosure('frontend.conf.template')],
        ];
    }
}
