<?php

namespace Mykolab\LaravelDockerBuilder\Tests\StubReplacers;

use Illuminate\Support\Facades\File;
use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\HasVolume;
use Mykolab\LaravelDockerBuilder\Facades\DockerAssets;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;
use Mykolab\LaravelDockerBuilder\StubReplacers\DockerFileStubReplacer;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class DockerFileStubReplacerTest extends TestCase
{
    /** @test */
    public function it_replace_service_template_to_service_yml_values_from_stubs(): void
    {
        $dockerServices = new DockerServiceCollection([
            $foo = new Foo(),
            $bar = new Bar(),
        ]);

        $dockerFileStubContent = File::get(
            DockerAssets::getDockerComposeYmlStubsPath('docker-compose')
        );

        $this->assertStringContainsString('{{volumes}}', $dockerFileStubContent);
        $this->assertStringContainsString('{{services}}', $dockerFileStubContent);

        File::partialMock()
            ->shouldReceive('get')
            ->andReturnValues([
                $foo->getYmlMock(),
                $bar->getYmlMock(),
            ]);

        $dockerFileStubReplacer = new DockerFileStubReplacer();
        $dockerComposeYmlContent = $dockerFileStubReplacer->replace($dockerFileStubContent, $dockerServices);

        $this->assertStringNotContainsString('{{volumes}}', $dockerComposeYmlContent);
        $this->assertStringNotContainsString('{{services}}', $dockerComposeYmlContent);
        $this->assertStringContainsString('services:', $dockerComposeYmlContent);

        try {
            $dockerComposeConfiguration = Yaml::parse($dockerComposeYmlContent);
            $servicesFromYmlConfiguration = array_keys($dockerComposeConfiguration['services']);
            $this->assertSame($dockerServices->map->getName()->toArray(), $servicesFromYmlConfiguration);
        } catch (ParseException $e) {
            $this->fail('Docker compose yml content is not valid.');
        }
    }

    /** @test */
    public function it_skip_volume_template_when_all_services_dont_have_volume(): void
    {
        $dockerServices = new DockerServiceCollection([
            $foo = new Foo(),
            $bar = new Bar(),
        ]);

        $dockerFileStubContent = File::get(
            DockerAssets::getDockerComposeYmlStubsPath('docker-compose')
        );

        File::partialMock()
            ->shouldReceive('get')
            ->andReturnValues([
                $foo->getYmlMock(),
                $bar->getYmlMock(),
            ]);

        $dockerFileStubReplacer = new DockerFileStubReplacer();
        $dockerComposeYmlContent = $dockerFileStubReplacer->replace($dockerFileStubContent, $dockerServices);

        $this->assertStringNotContainsString('{{volumes}}', $dockerComposeYmlContent);
    }

    /** @test */
    public function it_replace_volume_template_to_volume_values_from_docker_services(): void
    {
        $dockerServices = new DockerServiceCollection([
            $foo = new Foo(),
            $bar = new Bar(),
            $baz = new Baz(),
            $xyz = new Xyz(),
        ]);

        $dockerFileStubContent = File::get(
            DockerAssets::getDockerComposeYmlStubsPath('docker-compose')
        );

        $this->assertStringContainsString('{{volumes}}', $dockerFileStubContent);
        $this->assertStringContainsString('{{services}}', $dockerFileStubContent);

        File::partialMock()
            ->shouldReceive('get')
            ->andReturnValues([
                $foo->getYmlMock(),
                $bar->getYmlMock(),
                $baz->getYmlMock(),
                $xyz->getYmlMock(),
            ]);

        $dockerFileStubReplacer = new DockerFileStubReplacer();
        $dockerComposeYmlContent = $dockerFileStubReplacer->replace($dockerFileStubContent, $dockerServices);

        $this->assertStringContainsString('volumes:', $dockerComposeYmlContent);
        $this->assertStringContainsString("baz:\n    test1: test1val", $dockerComposeYmlContent);
        $this->assertStringContainsString("xyz:\n    test2: test2val", $dockerComposeYmlContent);
    }

    /** @test */
    public function it_has_valid_yml_stubs_for_all_available_services(): void
    {
        /** @var DockerServiceFactory $dockerServiceFactory */
        $dockerServiceFactory = $this->app->make(DockerServiceFactory::class);
        $dockerServices = $dockerServiceFactory->getDockerServices();

        $dockerFileStubContent = File::get(
            DockerAssets::getDockerComposeYmlStubsPath('docker-compose')
        );

        $dockerFileStubReplacer = new DockerFileStubReplacer();
        $dockerComposeYmlContent = $dockerFileStubReplacer->replace($dockerFileStubContent, $dockerServices);

        try {
            $dockerComposeConfiguration = Yaml::parse($dockerComposeYmlContent);
            $servicesFromYmlConfiguration = array_keys($dockerComposeConfiguration['services']);
            $this->assertSame($dockerServices->map->getName()->toArray(), $servicesFromYmlConfiguration);
        } catch (ParseException $e) {
            $this->fail('Docker compose yml content is not valid.');
        }
    }
}

class Foo implements DockerService {

    public function getName(): string
    {
        return 'foo';
    }

    public function getYmlMock(): string
    {
        return <<<END
          foo:
            image: foo
        END;
    }
}

class Bar implements DockerService {

    public function getName(): string
    {
        return 'bar';
    }

    public function getYmlMock(): string
    {
        return <<<END
          bar:
            image: foo
        END;
    }
}

class Baz implements DockerService, HasVolume {

    public function getName(): string
    {
        return 'baz';
    }

    public function getYmlMock(): string
    {
        return <<<END
          baz:
            image: foo
        END;
    }

    public function getVolumeOptions(): array
    {
        return [
            'test1' => 'test1val',
        ];
    }
}

class Xyz implements DockerService, HasVolume {

    public function getName(): string
    {
        return 'xyz';
    }

    public function getYmlMock(): string
    {
        return <<<END
          xyz:
            image: foo
        END;
    }

    public function getVolumeOptions(): array
    {
        return [
            'test2' => 'test2val',
        ];
    }
}
