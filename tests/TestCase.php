<?php

namespace Mykolab\LaravelDockerBuilder\Tests;

use Illuminate\Support\Facades\File;
use Orchestra\Testbench\TestCase as Orchestra;
use Mykolab\LaravelDockerBuilder\LaravelDockerBuilderServiceProvider;

class TestCase extends Orchestra
{
    protected string $basePath = __DIR__;

    protected function getPackageProviders($app)
    {
        return [
            LaravelDockerBuilderServiceProvider::class,
        ];
    }

    protected function getTestFilesPath(?string $relativePath = null): string
    {
        return rtrim(__DIR__.'/test-files/'.$relativePath, '/');
    }

    protected function getTempDirPath(?string $relativePath = null): string
    {
        return rtrim($this->getTestFilesPath('temp/'.$relativePath), '/');
    }

    protected function emptyTempDirectory()
    {
        $tempDirPath = $this->getTestFilesPath('temp');

        $files = scandir($tempDirPath);

        foreach ($files as $file) {
            if (! in_array($file, ['.', '..', '.gitignore'])) {
                $path = "{$tempDirPath}/{$file}";
                if (File::isDirectory($path)) {
                    File::deleteDirectory($path);
                } elseif (File::isFile($path)) {
                    File::delete($path);
                }
            }
        }
    }
}
