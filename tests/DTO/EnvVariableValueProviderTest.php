<?php

namespace Mykolab\LaravelDockerBuilder\Tests\DTO;

use Illuminate\Translation\Translator;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class EnvVariableValueProviderTest extends TestCase
{
    /** @test */
    public function it_resolves_description_and_note_from_locale_files(): void
    {
        $expectedString = 'bar';
        $translatorMock = $this->mock(Translator::class);
        $translatorMock->shouldReceive('has')->twice()->andReturnTrue();
        $translatorMock->shouldReceive('get')->twice()->andReturn($expectedString);
        $this->app->instance('translator', $translatorMock);

        $envVariableValueProvider = EnvVariableValueProvider::make('foo');

        $this->assertSame($expectedString, $envVariableValueProvider->getDescription());
        $this->assertSame("# $expectedString\n", $envVariableValueProvider->getNote());
    }

    /** @test */
    public function it_overrides_resolved_description_and_note(): void
    {
        $expectedString = 'bar';
        $translatorMock = $this->mock(Translator::class);
        $translatorMock->shouldReceive('has')->twice()->andReturnTrue();
        $translatorMock->shouldReceive('get')->twice()->andReturn($expectedString);
        $this->app->instance('translator', $translatorMock);

        $envVariableValueProvider = EnvVariableValueProvider::make('foo')
            ->description('baz')
            ->note('xyz');

        $this->assertSame('baz', $envVariableValueProvider->getDescription());
        $this->assertSame("# xyz\n", $envVariableValueProvider->getNote());
    }
}
