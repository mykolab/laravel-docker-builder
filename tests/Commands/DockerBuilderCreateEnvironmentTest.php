<?php

namespace Mykolab\LaravelDockerBuilder\Tests\Commands;

use Mykolab\LaravelDockerBuilder\AssetGenerators\EnvFileGenerator;
use Mykolab\LaravelDockerBuilder\DTO\EnvVariableValueProvider;
use Mykolab\LaravelDockerBuilder\EnvironmentActionDispatcher;
use Mykolab\LaravelDockerBuilder\EnvironmentConfigurator;
use Mykolab\LaravelDockerBuilder\EnvironmentService;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class DockerBuilderCreateEnvironmentTest extends TestCase
{
    /** @test */
    public function it_exit_with_failure_command_when_environment_with_same_name_already_installed(): void
    {
        $environmentServiceMock = $this->mock(EnvironmentService::class);
        $environmentServiceMock->expects('isEnvironmentExists')->andReturnTrue();

        $this->swap(EnvironmentService::class, $environmentServiceMock);

        $this
            ->artisan('docker-builder:create-environment')
            ->expectsQuestion(
                'Enter environment name (it should be sluggable word, ex.: <fg=yellow>local</>, <fg=yellow>staging</>, <fg=yellow>production</>)',
                'foo'
            )
            ->expectsOutput("Cannot create 'foo' environment. It is already exists.")
            ->assertFailed();
    }

    /** @test */
    public function it_asks_for_input_environment_variable_value(): void
    {
        $envConfiguratorMock = $this->mock(EnvironmentConfigurator::class);
        $envConfiguratorMock
            ->shouldReceive('nextInputableVariableValueProvider')
            ->twice()
            ->andReturns(
                EnvVariableValueProvider::make('foo')->description('enter value')->requireInput(),
                null
            );
        $envConfiguratorMock->expects('addEnvVariable')->andReturnUndefined();
        $envConfiguratorMock->shouldReceive('getEnvironment');

        $environmentServiceMock = $this->mock(EnvironmentService::class);
        $environmentServiceMock->expects('newEnvironmentConfigurator')->andReturn($envConfiguratorMock);
        $environmentServiceMock->expects('isEnvironmentExists')->andReturnFalse();
        $this->swap(EnvironmentService::class, $environmentServiceMock);

        $envFileGeneratorMock = $this->mock(EnvFileGenerator::class);
        $envFileGeneratorMock->shouldReceive('generate')->andReturnTrue();
        $this->swap(EnvFileGenerator::class, $envFileGeneratorMock);

        $environmentActionDispatcherMock = $this->mock(EnvironmentActionDispatcher::class);
        $environmentActionDispatcherMock->shouldReceive('dispatchAfterEnvironmentCreatedAction')->andReturnUndefined();
        $this->swap(EnvironmentActionDispatcher::class, $environmentActionDispatcherMock);

        $this
            ->artisan('docker-builder:create-environment')
            ->expectsQuestion(
                'Enter environment name (it should be sluggable word, ex.: <fg=yellow>local</>, <fg=yellow>staging</>, <fg=yellow>production</>)',
                'bar'
            )
            ->expectsQuestion('enter value [<fg=yellow>foo</>]', 'baz')
            ->assertSuccessful();
    }

    /** @test */
    public function it_asks_to_choose_environment_variable_value(): void
    {
        $envConfiguratorMock = $this->mock(EnvironmentConfigurator::class);
        $envConfiguratorMock
            ->shouldReceive('nextInputableVariableValueProvider')
            ->twice()
            ->andReturns(
                EnvVariableValueProvider::make('foo')->description('enter value')->choices(['bar', 'baz']),
                null
            );
        $envConfiguratorMock->expects('addEnvVariable')->andReturnUndefined();
        $envConfiguratorMock->shouldReceive('getEnvironment');

        $environmentServiceMock = $this->mock(EnvironmentService::class);
        $environmentServiceMock->expects('newEnvironmentConfigurator')->andReturn($envConfiguratorMock);
        $environmentServiceMock->expects('isEnvironmentExists')->andReturnFalse();
        $this->swap(EnvironmentService::class, $environmentServiceMock);

        $envFileGeneratorMock = $this->mock(EnvFileGenerator::class);
        $envFileGeneratorMock->shouldReceive('generate')->andReturnTrue();
        $this->swap(EnvFileGenerator::class, $envFileGeneratorMock);
        $environmentActionDispatcherMock = $this->mock(EnvironmentActionDispatcher::class);
        $environmentActionDispatcherMock->shouldReceive('dispatchAfterEnvironmentCreatedAction')->andReturnUndefined();
        $this->swap(EnvironmentActionDispatcher::class, $environmentActionDispatcherMock);

        $this
            ->artisan('docker-builder:create-environment')
            ->expectsQuestion(
                'Enter environment name (it should be sluggable word, ex.: <fg=yellow>local</>, <fg=yellow>staging</>, <fg=yellow>production</>)',
                'xyz'
            )
            ->expectsChoice('enter value [<fg=yellow>foo</>]', 'baz', ['bar', 'baz'], true)
            ->assertSuccessful();
    }

    /** @test */
    public function it_fails_when_cannot_generate_env_file(): void
    {
        $envConfiguratorMock = $this->mock(EnvironmentConfigurator::class);
        $envConfiguratorMock
            ->expects('nextInputableVariableValueProvider')
            ->andReturnNull();
        $envConfiguratorMock->shouldReceive('getEnvironment');

        $environmentServiceMock = $this->mock(EnvironmentService::class);
        $environmentServiceMock->expects('newEnvironmentConfigurator')->andReturn($envConfiguratorMock);
        $environmentServiceMock->expects('isEnvironmentExists')->andReturnFalse();
        $this->swap(EnvironmentService::class, $environmentServiceMock);

        $envFileGeneratorMock = $this->mock(EnvFileGenerator::class);
        $envFileGeneratorMock->shouldReceive('generate')->andReturnFalse();

        $this
            ->artisan('docker-builder:create-environment')
            ->expectsQuestion(
                'Enter environment name (it should be sluggable word, ex.: <fg=yellow>local</>, <fg=yellow>staging</>, <fg=yellow>production</>)',
                'foo'
            )
            ->expectsOutput('Generating environment files...')
            ->expectsOutput('Cannot generate env file.')
            ->assertFailed();
    }

    /** @test */
    public function it_dispatches_after_env_created_actions_when_env_file_was_generated_successfully(): void
    {
        $envConfiguratorMock = $this->mock(EnvironmentConfigurator::class);
        $envConfiguratorMock
            ->expects('nextInputableVariableValueProvider')
            ->andReturnNull();
        $envConfiguratorMock->shouldReceive('getEnvironment');

        $environmentServiceMock = $this->mock(EnvironmentService::class);
        $environmentServiceMock->expects('newEnvironmentConfigurator')->andReturn($envConfiguratorMock);
        $environmentServiceMock->expects('isEnvironmentExists')->andReturnFalse();
        $this->swap(EnvironmentService::class, $environmentServiceMock);

        $envFileGeneratorMock = $this->mock(EnvFileGenerator::class);
        $envFileGeneratorMock->shouldReceive('generate')->andReturnTrue();

        $environmentActionDispatcherMock = $this->mock(EnvironmentActionDispatcher::class);
        $environmentActionDispatcherMock->expects('dispatchAfterEnvironmentCreatedAction')->andReturnUndefined();
        $this->swap(EnvironmentActionDispatcher::class, $environmentActionDispatcherMock);

        $this
            ->artisan('docker-builder:create-environment')
            ->expectsQuestion(
                'Enter environment name (it should be sluggable word, ex.: <fg=yellow>local</>, <fg=yellow>staging</>, <fg=yellow>production</>)',
                'foo'
            )
            ->expectsOutput('Generating environment files...')
            ->expectsOutput('Environment foo was successfully created.')
            ->assertSuccessful();
    }
}
