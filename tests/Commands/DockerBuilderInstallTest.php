<?php

namespace Mykolab\LaravelDockerBuilder\Tests\Commands;

use Mockery;
use Mykolab\LaravelDockerBuilder\AssetGenerators\CommonFilesGenerator;
use Mykolab\LaravelDockerBuilder\AssetGenerators\DockerFilesGenerator;
use Mykolab\LaravelDockerBuilder\Collections\DockerServiceCollection;
use Mykolab\LaravelDockerBuilder\Contracts\DockerService\DockerService;
use Mykolab\LaravelDockerBuilder\Exceptions\DockerBuilderException;
use Mykolab\LaravelDockerBuilder\Factories\DockerServiceFactory;
use Mykolab\LaravelDockerBuilder\Tests\TestCase;

class DockerBuilderInstallTest extends TestCase
{
    /** @test */
    public function it_asks_for_using_default_docker_services_configuration(): void
    {
        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock
            ->expects('getDefaultDockerServices')
            ->andReturn(
                new DockerServiceCollection([
                    new Foo(),
                    new Bar(),
                ])
            );

        $this->swap(DockerServiceFactory::class, $dockerServiceFactoryMock);
        $this->swapMocks();

        $this->artisan('docker-builder:install')
            ->expectsConfirmation('Are you sure you want to install docker services with default configuration: <fg=yellow>foo, bar</> ?', 'yes')
            ->expectsConfirmation('Would you like to create new environment?')
            ->assertSuccessful();
    }

    /** @test */
    public function it_asks_to_choose_custom_docker_services_configuration(): void
    {
        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock
            ->expects('getDefaultDockerServices')
            ->andReturn(
                new DockerServiceCollection([
                    new Foo(),
                    new Bar(),
                ])
            );

        $dockerServiceFactoryMock
            ->expects('getDockerServices')
            ->andReturn(
                new DockerServiceCollection([
                    new Foo(),
                    new Bar(),
                    new Baz(),
                ])
            );

        $this->swap(DockerServiceFactory::class, $dockerServiceFactoryMock);

        $dockerFilesGeneratorMock = $this->mock(DockerFilesGenerator::class);
        $dockerFilesGeneratorMock
            ->expects('generate')
            ->with(
                Mockery::on(
                    function (DockerServiceCollection $dockerServices) {
                        $this->assertEquals([new Foo(), new Baz()], $dockerServices->toArray());

                        return 2 === $dockerServices->count();
                    }
                )
            )
            ->andReturnUndefined();

        $commonFilesGeneratorMock = $this->mock(CommonFilesGenerator::class);
        $commonFilesGeneratorMock
            ->expects('generate')
            ->andReturnUndefined();

        $this->swap(DockerFilesGenerator::class, $dockerFilesGeneratorMock);
        $this->swap(CommonFilesGenerator::class, $commonFilesGeneratorMock);

        $this->artisan('docker-builder:install')
            ->expectsConfirmation('Are you sure you want to install docker services with default configuration: <fg=yellow>foo, bar</> ?')
            ->expectsChoice('Choose docker services:', ['foo', 'baz'], ['foo', 'bar', 'baz'])
            ->expectsConfirmation('Are you sure you want to install such docker services: <fg=yellow>foo,baz</> ?', 'yes')
            ->expectsConfirmation('Would you like to create new environment?')
            ->assertSuccessful();
    }

    /** @test */
    public function it_asks_again_to_choose_custom_docker_services_configuration_when_configuration_not_confirmed(): void
    {
        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock
            ->expects('getDefaultDockerServices')
            ->andReturn(
                new DockerServiceCollection([
                    new Foo(),
                    new Bar(),
                ])
            );

        $dockerServiceFactoryMock
            ->shouldReceive('getDockerServices')
            ->twice()
            ->andReturn(
                new DockerServiceCollection([
                    new Foo(),
                    new Bar(),
                    new Baz(),
                ])
            );

        $this->swap(DockerServiceFactory::class, $dockerServiceFactoryMock);
        $dockerFilesGeneratorMock = $this->mock(DockerFilesGenerator::class);
        $dockerFilesGeneratorMock
            ->expects('generate')
            ->with(
                Mockery::on(
                    function (DockerServiceCollection $dockerServices) {
                        $this->assertEquals([new Foo()], $dockerServices->toArray());

                        return 1 === $dockerServices->count();
                    }
                )
            )
            ->andReturnUndefined();

        $commonFilesGeneratorMock = $this->mock(CommonFilesGenerator::class);
        $commonFilesGeneratorMock
            ->expects('generate')
            ->andReturnUndefined();

        $this->swap(DockerFilesGenerator::class, $dockerFilesGeneratorMock);
        $this->swap(CommonFilesGenerator::class, $commonFilesGeneratorMock);

        $this->artisan('docker-builder:install')
            ->expectsConfirmation('Are you sure you want to install docker services with default configuration: <fg=yellow>foo, bar</> ?')
            ->expectsChoice('Choose docker services:', ['foo', 'baz'], ['foo', 'bar', 'baz'])
            ->expectsConfirmation('Are you sure you want to install such docker services: <fg=yellow>foo,baz</> ?')

            ->expectsChoice('Choose docker services:', ['foo'], ['foo', 'bar', 'baz'])
            ->expectsConfirmation('Are you sure you want to install such docker services: <fg=yellow>foo</> ?', 'yes')

            ->expectsConfirmation('Would you like to create new environment?')
            ->assertSuccessful();
    }

    /** @test */
    public function it_exit_command_with_fail_when_docker_destination_folder_is_already_exists(): void
    {
        $dockerServiceFactoryMock = $this->mock(DockerServiceFactory::class);
        $dockerServiceFactoryMock
            ->expects('getDefaultDockerServices')
            ->andReturn(
                new DockerServiceCollection([
                    new Foo(),
                    new Bar(),
                ])
            );
        $this->swap(DockerServiceFactory::class, $dockerServiceFactoryMock);

        $dockerFilesGeneratorMock = $this->mock(DockerFilesGenerator::class);
        $dockerFilesGeneratorMock
            ->expects('generate')
            ->andThrowExceptions([new DockerBuilderException('Docker configuration already exists.')]);

        $this->artisan('docker-builder:install')
            ->expectsConfirmation('Are you sure you want to install docker services with default configuration: <fg=yellow>foo, bar</> ?', 'yes')
            ->assertFailed();
    }

    /**
     * @return void
     */
    private function swapMocks(): void
    {
        $dockerFilesGeneratorMock = $this->mock(DockerFilesGenerator::class);
        $dockerFilesGeneratorMock
            ->expects('generate')
            ->andReturnUndefined();

        $commonFilesGeneratorMock = $this->mock(CommonFilesGenerator::class);
        $commonFilesGeneratorMock
            ->expects('generate')
            ->andReturnUndefined();

        $this->swap(DockerFilesGenerator::class, $dockerFilesGeneratorMock);
        $this->swap(CommonFilesGenerator::class, $commonFilesGeneratorMock);
    }
}

class Foo implements DockerService {

    public function getName(): string
    {
        return 'foo';
    }
}

class Bar implements DockerService {

    public function getName(): string
    {
        return 'bar';
    }
}

class Baz implements DockerService {

    public function getName(): string
    {
        return 'baz';
    }
}
