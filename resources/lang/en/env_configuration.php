<?php

return [
    'variables' => [
        'description' => [
            'COMPOSE_PROJECT_NAME' => 'Enter your project name',
            'IMAGE_REGISTRY' => 'Enter your docker registry url',
            'PHP_VERSION' => 'Choose php version:',

            'BACKEND_APP_DOMAINS' => 'Enter backend app domains <fg=white>(they should be separated with space sign)</>',
            'FRONTEND_APP_DOMAINS' => 'Enter frontend app domains <fg=white>(they should be separated with space sign)</>',

            'CERTBOT_EMAIL' => 'Enter your email for certbot configuration',

            'POSTGRES_DB' => 'Enter database name',
            'POSTGRES_USER' => 'Enter database user',
            'POSTGRES_PASSWORD' => 'Enter user password',

            'REDIS_PASSWORD' => 'Enter redis password',
        ],
        'note' => [
            'PHP_VERSION' => 'Available php versions: :versions',
            'INSTALL_PGSQL' => 'Next variables will be applied to all services where using php (workspace, php-fpm, horizon)',
        ],
    ],
];
