## Docker builder with preset configuration for Laravel

Package which generate docker compose configuration.
How to use:
1. Run command `php artisan docker-builder:install` and select docker services what you need.
2. Then create environment (for example **local**)
3. After that package will copy docker files and generate docker-compose.yml in `docker` folder.
4. Copy .env, for example: `cp .env.local .env`
5. Run `docker-compose up -d`

To create new environment you can run command `php artisan docker-builder:create-environment` after filling all asked questions it will generate new env file.

### Note
This package is able to generate docker configuration for staging or production environment.
If `certbot` service is installed package will add `deploy.sh` and `init-letsencrypt.sh` files. Which can automatically create SSL certificate without additional actions.
But it is not tested properly yet. So for now, this package is better to use for local development.
